import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PageState } from '../enums/page.enum';
import { Role } from '../enums/role.enum';
import { User } from '../interfaces/user.interface';
import { AuthenticationService } from '../services/authentication.service';
import { ErrorStateMatcherService } from '../services/error-state-matcher.service';
import { FireAlertService } from '../services/fire-alert.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFailed: boolean = false;
  registerFailed: boolean = false;
  pageState: PageState = PageState.LOGIN;

  //validator-login
  loginForm: FormGroup;

  //validator-register
  registerForm: FormGroup;
  
  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private userService: UserService,
    private matcher: ErrorStateMatcherService,
    private formBuilder: FormBuilder,
    private fireAlertService: FireAlertService
  ) {
    this.loginForm = this.formBuilder.group({
      loginUsername: ["", [Validators.required]],
      loginPassword: ["", [Validators.required]],
    });

    this.registerForm = this.formBuilder.group(
      {
        registerUsername: ["", [Validators.required]],
        registerPassword: ["", [Validators.required]],
        confirmPassword: [""],
      },
      { validator: this.checkPasswords }
    );
  }

  ngOnInit(): void {
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.registerPassword.value;
    let confirm = group.controls.confirmPassword.value;
    return pass === confirm ? null : { notSame: true };
  }

  isLoginRequiredError(): boolean {
    if (this.loginForm.hasError("required", "loginUsername")) {
      return true;
    } else if (this.loginForm.hasError("required", "loginPassword")) {
      return true;
    } else {
      return false;
    }
  }

  isRegisterRequiredError(): boolean {
    if (this.registerForm.hasError("required", "registerUsername")) {
      // alert("Password field is required");
      this.fireAlertService.fireInputWarning("Username field is required");
      return true;
    }
    if (this.registerForm.hasError("required", "registerPassword")) {
      // alert("Password field is required");
      this.fireAlertService.fireInputWarning("Password field is required");
      return true;
    } else if (this.registerForm.hasError("notSame")) {
      // alert("Password and Confirm are not the same");
      this.fireAlertService.fireInputNotMatchWarning();
      return true;
    } else {
      return false;
    }
  }

  loginPage() {
    console.log("login page loading");
    this.pageState = PageState.LOGIN;
  }

  registerPage() {
    console.log("register page loading");
    this.pageState = PageState.REGISTER;
  }

  onLogin(username, password) {
    console.log(username, password);

    if (!this.isLoginRequiredError()) {
      this.authService.OnLogin({ username, password }).subscribe(
        (data: User) => {
          console.log(data);
          console.log("login");
          this.loginFailed = false;
          // alert('login!');

          this.userService.userLogin(data);
          this.navigateHomePage();
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          if (error.status === 403) {
            console.log("can not login. your inputs are incorrect");

            this.fireAlertService.fireIncorrectInputWarning(
              "can not login. your inputs are incorrect"
            );

            this.loginFailed = true;
          }
        }
      );
    } else {
      // alert("All fields are required");
      this.fireAlertService.fireInputWarning("All fields are required");
    }
  }

  onRegister(username: string, password: string) {
    if (!this.isRegisterRequiredError()) {
      this.authService
        .OnRegister({
          username: username,
          password: password,
          type: Role.JUNIOR,
        })
        .subscribe(
          (data: User) => {
            console.log({ ...data });
            console.log("register");
            this.registerFailed = false;
            // alert("register!");

            this.fireAlertService.fireSuccess(
              "Successfully registered into the system"
            );
          },
          (error: HttpErrorResponse) => {
            console.log(error);
            if (error.status === 400) {
              console.log("Username already exists");
              this.registerFailed = true;
              // alert("user already exists");

              this.fireAlertService.fireCreateError(username);
            }
          }
        );
    }
  }

  navigateHomePage() {
    let url;
    if (this.userService.isAdmin())
      url = 'admin';
    else
      url = 'guest';
    this.router.navigate([url]);
  }
}