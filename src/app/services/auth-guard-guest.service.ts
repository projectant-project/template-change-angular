import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router/router';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuestService {

  authRoutes = [
    'guest',
    'dashboard',
    'icons',
    'live',
    'home',
    'profile-edit',
    'statistics',
  ];

  constructor(private userService: UserService, private router: Router) { }

  canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    console.log('can activate function is called', route.url);

    if (this.userService.isAdmin())
    {
      console.log('guest unauthorized, navigating admin/unauthorized')
      this.router.navigate(['unauthorized']);
      return true;
    }

    else if (route.url.length === 0)
      return true;

    console.log(route.url[0].path, (this.authRoutes.includes(route.url[0].path)) ? 'Auth' : 'Denied')

    return this.authRoutes.includes(route.url[0].path);
  }
}
