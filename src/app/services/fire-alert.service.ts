import { Injectable } from "@angular/core";
import Swal from "sweetalert2";

@Injectable({
  providedIn: "root",
})
export class FireAlertService {
  constructor() {}

  fireDbError(operation: string, element: string) {
    Swal.fire({
      title: "Error Occured",
      icon: "error",
      text: `Error occured while trying to ${operation} your ${element} in database`,
    });
  }

  fireCreateError(element: string) {
    Swal.fire({
      title: "Input Error",
      icon: "error",
      text: element + " is already exists",
    });
  }

  fireInputWarning(msg: string) {
    Swal.fire({
      title: "Required field",
      icon: "warning",
      text: msg,
    });
  }

  fireIncorrectInputWarning(msg: string) {
    Swal.fire({
      title: "Incorrect inputs",
      icon: "warning",
      text: msg,
    });
  }

  fireInputNotMatchWarning() {
    Swal.fire({
      title: "Unmatched fields",
      icon: "warning",
      text: "Password and Confirm are not the same",
    });
  }

  fireSuccess(msg: string) {
    Swal.fire({
      title: msg,
      icon: "success",
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      position: "top",
    });
  }

  fireNoData() {
    Swal.fire({
      title: "No Data",
      icon: "warning",
      text: "No data found in db",
      showConfirmButton: false,
      timer: 2500,
      timerProgressBar: true,
      position: "top",
    });
  }

  fireUnknownError() {
    Swal.fire({
      title: "Error Occured",
      icon: "error",
      text: `Unknown error occured`,
    });
  }

  // fireCreationCanceled(element: string){
  //   Swal.fire({
  //     title: `${element[0].toUpperCase()}${element.slice(1)} creation was canceled.`,
  //     icon: 'warning',
  //     showConfirmButton: false,
  //     timer: 1500,
  //     timerProgressBar: true,
  //     position: 'top-end',
  //   });
  // }

  // fireDeleteCanceled(element: string){
  //   Swal.fire({
  //     title: `${element[0].toUpperCase()}${element.slice(1)} delete was canceled.`,
  //     icon: 'warning',
  //     showConfirmButton: false,
  //     timer: 1500,
  //     timerProgressBar: true,
  //     position: 'top-end',
  //   });
  // }
}
