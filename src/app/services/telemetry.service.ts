import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Range } from '../interfaces/range.interface';
import { Sensor } from '../interfaces/sensor.interface';
import { Telemetry } from '../interfaces/telemetry.interface';

@Injectable({
  providedIn: 'root'
})
export class TelemetryService {
  telemetryUrl: string = 'https://localhost:6001/TelemetryApi';
  
  private startRanges: Range[] = [
    {
      time: '1d',
      full: '1 Day'
    },
    {
      time: '3d',
      full: '3 Days'
    },
    {
      time: '1w',
      full: '1 Week'
    },
    {
      time: '1m',
      full: '1 Month'
    }

  ];

  private sensors = [
    {key: 'ACCL', name: 'Accelerometer'},
    {key: 'GYRO', name: 'Gyroscope'},
    {key: 'SHUT', name: 'Shutter speed'},
    {key: 'GPS5', name: 'Lat.'},
    {key: 'GPS5', name: 'Long.'},
    {key: 'GPS5', name: 'Alt.'},
    {key: 'GPS5', name: '2D speed'},
  ]

  constructor(private http: HttpClient) { }

  HttpTelemetryGet(device: string, key: string, filter: {name: string, time: string, stop: string}): Observable<Telemetry[]> {
    console.log('trying to get datas');
      
    return this.http.get<Telemetry[]>(`${this.telemetryUrl}/${device}/${key}`, {params: filter});
  }

  HttpSensorsGet(device: string): Observable<Sensor[]> {
    console.log('trying to get sensors');
      
    return this.http.get<Sensor[]>(`${this.telemetryUrl}/${device}`);
  }

  GetStartRanges()
  {
    return this.startRanges;
  }

  GetDefaultSensors(){
    return this.sensors;
  }
}
