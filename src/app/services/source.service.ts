import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { Source } from '../interfaces/source.interface';
import { UpdateResponse } from '../interfaces/update-response.interface';


@Injectable({
  providedIn: 'root'
})
export class SourceService {

  sources: Source[] = [];
  private sourceUrl: string = 'http://localhost:5000/sources';

  constructor(private http: HttpClient) {  }
  
  setSources(sources: Source[]) {
    this.sources = sources;
  }

  getSources(): Source[]{
    console.log('getting sources', this.sources, this.sources.length)
    if (this.sources.length === 0)
    {
      console.log('fetching sources')
      this.HttpGetSources().subscribe((sources: Source[]) => {
        this.sources = sources;

        this.SortSources();
        console.log(this.sources)
      });
    }
    else{
      console.log('sources is already fetched')
    }
    
    // this.SortSources();
    return this.sources;
  }

    // sources
    HttpGetSources(): Observable<Source[]> {
      console.log('trying to get sources');
      
      return this.http.get<Source[]>(this.sourceUrl);
    }
  
    HttpUpdateManySources(sources: string[], title: string, discription: string) {
      console.log('trying to update all');
      
      console.log(title, discription);
      return this.http.put(this.sourceUrl, {"sources":sources, "title": title, "description": discription}, { responseType: 'json'});
    }
  
    HttpUpdateOneSource(uri: string, title: string, discription: string) {
      console.log('trying to update one');
      
      console.log(uri, title);
      console.log(`${this.sourceUrl}/${uri}`);
      // return this.http.get<Source[]>(`${this.sourceUrl}/${uri}`, {responseType: 'json'});
      return this.http.put<UpdateResponse>(`${this.sourceUrl}/${uri}`, {"title": title, "description": discription}, { responseType: 'json'});
    }
    
    HttpCreateSource(source: Source): Observable<Source> {
      console.log('trying to send the server some data in order to create source');
      
      return this.http.post<Source>(this.sourceUrl, source, {responseType: 'json'});
    }
  
    HttpDeleteOneSource(uri: string){
      console.log('trying to delete one');
      console.log(uri);
  
      return this.http.delete(`${this.sourceUrl}/${uri}`);
    }
  
    HttpDeleteManySources(sources: string[]){
      console.log('trying to delete some');
      console.log(sources);
  
      // return this.http.delete(this.serverUrl);
      let parameters = new HttpParams();
      for(const source of sources){
        parameters = parameters.append('source', source)
      }
  
      console.log(parameters);
      return this.http.delete(this.sourceUrl, {params: parameters, responseType: 'json'});
    }

    DeleteSource(source: Source, index){
      console.log('deleting source');
      // let index = this.sources.indexOf(source);
      console.log(index);
      if (index != -1)
        this.sources.splice(index, 1);
      
      console.log(this.sources);
    }

    AppendSource(source: Source){
      this.sources.push(source);
      this.SortSources();
    }

    SortSources(){
      this.sources.sort((a, b) => (a.title > b.title) ? 1 : -1);
    }
}
