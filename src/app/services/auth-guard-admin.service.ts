import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router/router';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardAdminService implements CanActivate{

  authRoutes = [
    'admin',
    'dashboard',
    'icons',
    'live',
    'home',
    'user-management',
    'source-management' ,
    'statistics',
  ];

  constructor(private userService: UserService, private router: Router) { }

  canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    console.log('can activate function is called', route, route.url, state);

    if (!this.userService.isAdmin())
    {
      console.log('admin unauthorized')
      this.router.navigate(['unauthorized']);
      return false;
    }

    console.log(route.url[0].path, (this.authRoutes.includes(route.url[0].path)) ? 'Auth' : 'Denied')

    return this.authRoutes.includes(route.url[0].path);
    // throw new Error('Method not implemented.');
  }
}
