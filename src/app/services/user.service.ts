import { Injectable } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { Role } from '../enums/role.enum';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs';
import * as sha256 from 'hash.js/lib/hash/sha/256';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private serverUrl: string = 'http://localhost:3000/users';
  users: User[] = []
  user: User = null;
  username = 'username';
  type = 'type';
  password = 'password';

  admin: boolean;

  constructor(private http: HttpClient, private authService: AuthenticationService) {  }
  
  // setUser(user: User) {
  //   console.log('current user:', this.user);
  //   console.log(isNullOrUndefined(this.user) ? 'User is Null' : 'User is NOT Null')
  //   if (isNullOrUndefined(this.user))
  //     this.createCookie(user);
      
  //   this.user = user;
  // }

  // getUser(): User{
  //   let user: User = (isNullOrUndefined(this.user)) ? this.retrieveCookie() : this.user;
  //   if (isNullOrUndefined(this.user))
  //     this.setUser(user);
  //   return user;
  // }

  getUser(): User{
    this.user = (isNullOrUndefined(this.user)) ? this.retrieveCookie() : this.user;
    return this.user;
  }

  isAdmin(): boolean{
    this.getUser();
    return this.user.type === Role.ADMIN ? true : false;
  }

  userLogin(user: User){
    this.user = user;
    this.createCookie(user);
  }

  createCookie(user: User){
    console.log(`Setting cookie:\n${this.username}:${user.username}, ${this.type}:${user.type}, ${this.password}:${user.password};`);
    document.cookie = `${this.username}:${user.username}, ${this.type}:${user.type}, ${this.password}:${user.password};`;
  }

  retrieveCookie(): User{
    let usernameIndex = this.username.length + 1;
    let typeIndex = this.type.length + 1;
    let passwordIndex = this.password.length + 1;

    let userCookies = document.cookie.split(';');
    console.log(userCookies);
    let userString = userCookies[userCookies.length - 1].replace(/\s/g,'');
    let userArr = userString.split(',');
    console.log(userString);

    let username = userArr[0].substring(usernameIndex);
    let type = +userArr[1].substring(typeIndex);
    let password = userArr[2].substring(passwordIndex);

    // console.log(`${username},${type},${password}`);
    console.log('User Retreived:', {username, type, password})
    return {username, type, password}
  }

  // retrieveCookie(): User{
  //   let usernameIndex = this.username.length + 1;
  //   let typeIndex = this.type.length + 1;
  //   let passwordIndex = this.password.length + 1;

  //   let userCookies = document.cookie.split(';');
  //   console.log('cookie retrevied:\n', userCookies)
  //   console.log('client0 :\n', userCookies[0])
  //   console.log('client1 :\n', userCookies[1])
  //   let userArr = userCookies[0].split(',');
  //   console.log('cookie retrevied:\n',userArr);

  //   let username = userArr[0].substring(usernameIndex);
  //   let type = +userArr[1].substring(typeIndex + 1);
  //   let password = userArr[2].substring(passwordIndex + 1);

  //   console.log(`${username},${type},${password}`);
  //   return {username, type, password}
  // }

  // setAdmin(admin: boolean){
  //   this.admin = admin;
  // }

  getUsers(){
    console.log('getting sources', this.users, this.users.length)
    if (this.users.length === 0)
    {
      console.log('fetching users')
      this.HttpGetUsers().subscribe((users: User[]) => {
        this.users = users;

        this.SortUsers();
        console.log(this.users)
      });
    }
    else{
      console.log('sources is already fetched')
    }
    
    // this.SortUsers();
    return this.users;
  }

  setUsers(users: User[]){
    this.users = users;
  }

  CreateUser(user: User): Observable<User> {
    return this.authService.OnRegister(user);
  }

  HttpGetUsers(): Observable<User[]> {
    console.log('trying to get users');

    return this.http.get<User[]>(this.serverUrl);
  }

  HttpUpdateManyUsers(password: string, users: User[]) {
    console.log('trying to update all');

    return this.http.put(this.serverUrl, {"password": this.authService.encrypt(password), "users": users}, { responseType: 'json'});
  }

  HttpUpdateOneUser(password: string, username: string) {
    console.log('trying to update one');

    return this.http.put(`${this.serverUrl}/${username}`, {"password": this.authService.encrypt(password)}, { responseType: 'json'});
  }

  HttpDeleteOneUser(username: string){
    console.log('trying to delete one');
    console.log(username);

    return this.http.delete(`${this.serverUrl}/${username}`);
  }

  HttpDeleteManyUsers(users: User[]){
    console.log('trying to delete some');
    console.log(users);

    // return this.http.delete(this.serverUrl);
    let parameters = new HttpParams();
    for(const user of users){
      parameters = parameters.append('user', user.username)
    }

    console.log(parameters);
    return this.http.delete(this.serverUrl, {params: parameters, responseType: 'json'});
  }

  OnPasswordChange(user: User){
    user.password = this.authService.encrypt(user.password);
    this.user = user;
  }

  DeleteUser(user: User, index){
    console.log('deleting user');
    // let index = this.users.indexOf(user);
    console.log(index);
    if (index != -1)
      this.users.splice(index, 1);
    
    console.log(this.users);
  }

  AppendUser(user: User){
    this.users.push(user);
    this.SortUsers();
  }

  SortUsers(){
    this.users.sort((a, b) => (a.username > b.username) ? 1 : -1);
  }

//   deleteAllCookies() {
//     var cookies = document.cookie.split(";");

//     for (var i = 0; i < cookies.length; i++) {
//         var cookie = cookies[i];
//         var eqPos = cookie.indexOf("=");
//         var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
//         document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
//     }
//   }
}
