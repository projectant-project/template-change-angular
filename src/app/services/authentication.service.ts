import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user.interface';
import * as sha256 from 'hash.js/lib/hash/sha/256';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  serverUrl: string = 'http://localhost:3000/users';

  constructor(private http: HttpClient) { }

  OnLogin(params: {username: string, password: string}): Observable<User> {
    console.log('trying to login');

    let parameters = new HttpParams()
    .append('username', params.username)
    .append('password', this.encrypt(params.password));
      
    return this.http.get<User>(`${this.serverUrl}/login`, {params: parameters, responseType: 'json'});
  }

  OnRegister(user: User): Observable<User> {
    console.log('trying to send the server some data');
    console.log(user.username, this.encrypt(user.password));
    user.password = this.encrypt(user.password);
    
    return this.http.post<User>(this.serverUrl, user, {responseType: 'json'});
  }

  encrypt(password: string): string {
    return sha256().update(password).digest('hex');
  }
}
