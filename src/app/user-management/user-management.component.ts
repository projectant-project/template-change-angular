import {
  Component,
  OnInit,
  ViewChild,
  ComponentFactoryResolver,
} from "@angular/core";
import { User } from "../interfaces/user.interface";
import { ErrorStateMatcherService } from "../services/error-state-matcher.service";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { MatSelectionList } from "@angular/material/list";
import { UpdateResponse } from "../interfaces/update-response.interface";
import { MatDialog } from "@angular/material/dialog";
import { DeleteUserDialogComponent } from "./dialogs/delete-user-dialog/delete-user-dialog.component";
import { startWith, debounceTime, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import { DeleteAllUsersDialogComponent } from "./dialogs/delete-all-users-dialog/delete-all-users-dialog.component";
import { AddUserDialogComponent } from "./dialogs/add-user-dialog/add-user-dialog.component";
import { isNullOrUndefined } from "util";
import { SelectOperation } from "../enums/selected.enum";
import { FireAlertService } from "../services/fire-alert.service";
import { MatCheckboxChange, MatCheckbox } from "@angular/material/checkbox";
import { UserService } from "../services/user.service";


class UserSelect {
  user: User;
  selected: boolean;

  constructor(user: User, selected: boolean) {
    this.user = user;
    this.selected = selected;
  }
}

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  @ViewChild("checkRef", { static: true }) checkRef;
  status: string = "Select All";
  users: UserSelect[] = [];
  myForm: FormGroup;
  hideCon = false;
  selectOperation: SelectOperation = SelectOperation.SELECT;
  checkSign = false;
  indeterminateSign = false;

  // serach
  tempUsers: UserSelect[] = [];
  userControl = new FormControl();
  search = new FormControl();

  $search = this.search.valueChanges.pipe(
    startWith(null),
    debounceTime(200),
    switchMap((res: string) => {
      if (!res) return of(this.users);

      res = res.toLowerCase();
      return of(
        this.users.filter((x) => x.user.username.toLowerCase().indexOf(res) >= 0)
      );
    })
  );

  constructor(
    private userService: UserService,
    private matcher: ErrorStateMatcherService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private fireAlertService: FireAlertService
  ) {
    this.myForm = this.formBuilder.group(
      {
        password: ["", [Validators.required]],
        confirmPassword: [""],
      },
      { validator: this.checkPasswords }
    );
  }

  ngOnInit() {

    if (this.userService.users.length === 0){
      this.userService.HttpGetUsers().subscribe((users: User[]) => {
               
        users = users.sort((a, b) => (a.username > b.username) ? 1 : -1);

        users.map((user: User) => {
          this.users.push(new UserSelect(user, false));
        });

        this.userService.setUsers(users);
      });
    }
    else{
      let users: User[] = this.userService.getUsers(); 
      console.log(users);
      users.map((user: User) => {
        this.users.push(new UserSelect(user, false));
      });
    }

    this.$search.subscribe((users: UserSelect[]) => {
      console.log(this.tempUsers, users);
      if (!isNullOrUndefined(this.tempUsers)) {
        if (!this.checkEqual(this.tempUsers, users)) {
          console.log("list changed !!");
          this.tempUsers = users;
          this.checkBoxState();
        }
      } else this.tempUsers = users;
    });
  }

  checkEqual(a1: any[], a2: any[]) {
    if (isNullOrUndefined(a1) || isNullOrUndefined(a2)) return false;

    if (a1.length !== a2.length) return false;

    for (let i = 0; i < a2.length; i++) {
      if (!a1.includes(a2[i])) return false;
    }

    console.log("somehow true");
    return true;
  }

  checkContain(a1: any[], a2: any[]) {
    // check if a1 contains a2
    if (isNullOrUndefined(a1) || isNullOrUndefined(a2)) return false;

    for (let i = 0; i < a2.length; i++) {
      if (!a1.includes(a2[i])) return false;
    }

    console.log("a1 contains a2");
    return true;
  }

  checkContainSome(a1: any[], a2: any[]) {
    // check if a1 contains some of a2
    if (isNullOrUndefined(a1) || isNullOrUndefined(a2)) return false;

    let counter: number = 0;
    for (let i = 0; i < a2.length; i++) {
      if (a1.includes(a2[i])) counter++;
    }

    return counter;
  }

  selectionChange(option: any) {
    console.log("selectionChange");
    console.log(option.value);

    console.log("temp sources: ", this.tempUsers);
    console.log("before", this.userControl.value);

    let value = this.userControl.value || [];
    option.value.selected = option.selected;
    if (option.selected) value.push(option.value);
    else value = value.filter((x: any) => x != option.value);

    // adding to source control
    this.userControl.setValue(value);
    console.log("after", this.userControl.value);

    // changing source selected state
    let index: number = this.users.findIndex(
      (x) => x.user.username === option.value.user.username
    );
    this.users[index].selected = option.selected;

    // check box state now
    this.checkBoxState();
  }

  checkBoxState() {
    this.checkAllChecked();
    if (!this.checkSign) {
      this.checkIndeterminate();
    }
  }

  changeToSelectMode() {
    this.status = "Select All";
    this.selectOperation = SelectOperation.SELECT;
  }

  changeToDeselectMode() {
    this.status = "Deselect All";
    this.selectOperation = SelectOperation.DESELECT;
  }

  checkAllChecked() {
    if (!isNullOrUndefined(this.userControl.value)) {
      if (this.checkContain(this.userControl.value, this.tempUsers)) {
        console.log(
          "all checked. changing status to deselect + operation to deselect + sign to v"
        );
        this.checkSign = true;
        this.indeterminateSign = false;
        this.changeToDeselectMode();
      } else {
        console.log(
          "not all selected. changing status to select + operation to select"
        );
        this.checkSign = false;
        this.changeToSelectMode();
      }
    }
  }

  checkIndeterminate() {
    if (
      isNullOrUndefined(this.userControl.value) ||
      this.userControl.value.length === 0
    ) {
      console.log("none checked.");
      this.indeterminateSign = false;
    } else {
      if (!this.checkContainSome(this.userControl.value, this.tempUsers)) {
        console.log("no one of the searched users are checked.");
        this.indeterminateSign = false;
      } else {
        console.log("more than one of the searched users are checked.");
        this.indeterminateSign = true;
      }
    }
  }

  allCheckedOperation() {
    console.log("all checked operation");
    this.changeAllState();
    this.updateAll();
    this.checkAllChecked();
  }

  changeAllState() {
    console.log("changeAllState");
    this.tempUsers.forEach((user: UserSelect) => {
      let index: number = this.users.findIndex(
        (x) => x.user.username === user.user.username
      );
      this.users[index].selected =
        this.selectOperation === SelectOperation.SELECT ? true : false;
      user.selected =
        this.selectOperation === SelectOperation.SELECT ? true : false;
      console.log(user);
    });
  }

  updateAll() {
    let value = this.userControl.value || [];
    console.log("selected all is:", this.selectOperation);
    if (this.selectOperation === SelectOperation.SELECT) {
      console.log("inserting all temp into user control");
      this.tempUsers.forEach((user: UserSelect) => {
        if (!value.includes(user)) value.push(user);
      });
    } else {
      console.log("removing all temp into user control");
      value = value.filter((user: any) => !this.tempUsers.includes(user));
    }

    this.userControl.setValue(value);
    console.log("after changing all:", this.userControl.value);
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirm = group.controls.confirmPassword.value;
    return pass === confirm ? null : { notSame: true };
  }

  submitPassword(password: string) {
    if (!this.isRequiredError()) {
      console.log(this.userControl.value[0]);
      if (this.userControl.value.length === 1) {
        this.userService
          .HttpUpdateOneUser(password, this.userControl.value[0].user.username)
          .subscribe((user: UpdateResponse) => {
            console.log(user);
            if (user.ok === 0) {
              this.fireAlertService.fireDbError("update", "password");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess(
                "User's password updated successfully"
              );
              // alert('all sources updated successfully');
            }
          });
      } else {

        this.userService
          .HttpUpdateManyUsers(password, this.userControl.value)
          .subscribe((users: UpdateResponse) => {
            console.log(users);
            if (users.ok === 0) {
              this.fireAlertService.fireDbError("update", "sources");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess(
                "Wanted users' password updated successfully"
              );
            }
          });
      }
    }
  }

  isRequiredError(): boolean {
    if (this.myForm.hasError("required", "password")) {
      this.fireAlertService.fireInputWarning("Password");
      // alert('Password field is required');
      return true;
    } else if (this.myForm.hasError("notSame")) {
      this.fireAlertService.fireInputNotMatchWarning();
      // alert('Password and Confirm are not the same');
      return true;
    } else {
      return false;
    }
  }

  onDelete(user: User): void {
    console.log("type is", user.type);
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      width: "300px",
      height: "300px",
      data: { username: user.username, type: user.type ? "GUEST" : "ADMIN" },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log(`delete user:${user.username}!`);
        this.userService
          .HttpDeleteOneUser(user.username)
          .subscribe((ok: number) => {
            if (ok === 0) {
              this.fireAlertService.fireDbError("delete", "user");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess("User deleted successfully");
              // alert('sources deleted successfully');
              this.removeUserFromList([user]);
              this.uncheckAll();
            }
          });
      } else {
        // this.fireAlertService.fireDeleteCanceled('Wanted source')
        console.log("user regret");
      }
    });
  }

  onDeleteAll() {
    const dialogRef = this.dialog.open(DeleteAllUsersDialogComponent, {
      width: "325px",
      height: "250px",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log("delete all users:");
        console.log("user control:", this.userControl.value);
        let users: User[] = [];
        for (const user of this.userControl.value) {
          users.push(user.user);
        }

        this.userService
          .HttpDeleteManyUsers(users)
          .subscribe((ok: number) => {
            if (ok === 0) {
              this.fireAlertService.fireDbError("delete", "users");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess(
                "Wanted users deleted successfully"
              );
              // alert('wanted sources deleted successfully');

              let usersToDel: User[] = [];
              for (const sourceToDel of this.userControl.value) {
                usersToDel.push(sourceToDel.source);
              }
              console.log(usersToDel);
              this.removeUserFromList(usersToDel);
              this.uncheckAll();
            }
          });
      } else {
        // this.fireAlertService.fireDeleteCanceled('Wanted sources')
        console.log("user regret");
      }
    });
  }

  onAddSource() {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: "300px",
      height: "450px",
    });

    dialogRef
      .afterClosed()
      .subscribe((result: { status: number; userAdded: User }) => {
        if (result.status === 200) {
          console.log(result.userAdded);
          this.addUserToList(result.userAdded);
        } else if (result.status === 201) {
          // this.fireAlertService.fireCreationCanceled('source')
          console.log("user regret");
        } else {
          this.fireAlertService.fireUnknownError();
          console.log("error occured");
        }
      });
  }

  addUserToList(user: User) {
    this.users.push(new UserSelect(user, false));

    this.users.sort((a, b) => (a.user.username > b.user.username) ? 1 : -1);

    this.userService.AppendUser(user);
  }

  removeUserFromList(usersToDelete: User[]) {

    console.log('-----', usersToDelete, this.users);
    for (const userDel of usersToDelete) {
      console.log("user", userDel);
      let i = this.findUser(userDel);
      console.log('found in index ', i)
      if (i != -1) {
        let userDiv: HTMLElement = document.getElementById("user-div-" + i);
        userDiv.classList.add("animation-delete");
        console.log(userDiv);
        setTimeout(() => {
          console.log("removed user:", this.users.splice(i, 1));
        }, 300);
        this.userService.DeleteUser(userDel, i);
      }
    }

    console.log("users after slicing:\n", this.users);
    console.log("user control is:", this.userControl.value);
    this.userControl.value.splice(0, this.userControl.value.length);
    console.log("after removing user control is:", this.userControl.value);
  }

  findUser(user: User){
    for (let index = 0; index < this.users.length; index++) {
      console.log(this.users[index].user.username, user.username, this.users[index].user.username === user.username)
      if (this.users[index].user.username === user.username)
        return index;
    }

    return -1;
  }

  uncheckAll(){
    console.log("uncheck");
    this.tempUsers.forEach((user: UserSelect) => {
      let index: number = this.users.findIndex(
        (x) => x.user.username === user.user.username
      );
      this.users[index].selected = false;
      user.selected =  false;
      this.indeterminateSign = false;
      this.changeToSelectMode();
      console.log(user);
    });
  }

  // updateTitle(title: string, description: string){
  //   console.log("update Title", this.sourceControl);
    
  //   this.sourceControl.value.forEach((source: UserSelect) => {
  //     let index: number = this.sources.findIndex(
  //       (x) => x.source.title === source.source.title
  //     );

  //     this.sources[index].source.description = description;
  //     this.sources[index].source.title = title;
  //     source.source.title = title;
  //     source.source.description = description;

  //     console.log(source);
  //   });

  //   this.sortSources();
  //   this.sourceService.SortSources();
  // }

  sortSources(){
    this.users.sort((a, b) => (a.user.username > b.user.username) ? 1 : -1);
  }
}
