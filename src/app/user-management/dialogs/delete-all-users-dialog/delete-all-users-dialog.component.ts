import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-all-users-dialog',
  templateUrl: './delete-all-users-dialog.component.html',
  styleUrls: ['./delete-all-users-dialog.component.css']
})
export class DeleteAllUsersDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteAllUsersDialogComponent>) { }

  ngOnInit() {
  }

}
