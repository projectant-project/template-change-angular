import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorStateMatcherService } from '../../../services/error-state-matcher.service';
import { Role } from '../../../enums/role.enum';
import { User } from '../../../interfaces/user.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { FireAlertService } from '../../../services/fire-alert.service';
import { UserService } from '../../../services/user.service';


@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.css']
})
export class AddUserDialogComponent implements OnInit {

  myForm: FormGroup;
  registerFailed = false;

  constructor(public dialogRef: MatDialogRef<AddUserDialogComponent>, private userService: UserService, private matcher: ErrorStateMatcherService, private formBuilder: FormBuilder, private fireAlertService: FireAlertService) {
    this.myForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['']
      }, {validator: this.checkPasswords});
  }

  ngOnInit() {
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirm = group.controls.confirmPassword.value;
    return pass === confirm ? null : { notSame: true};
  }

  isRegisterRequiredError(): boolean {
    if (this.myForm.hasError('required', 'username')){
      this.fireAlertService.fireInputWarning('Username')
      return true; 
    }
    if (this.myForm.hasError('required', 'password')){
      this.fireAlertService.fireInputWarning('Password')
      return true; 
    }
    else if (this.myForm.hasError('notSame')){
      this.fireAlertService.fireInputNotMatchWarning();
      return true;
    }
    else{
      return false;
    }
  }
  
  onRegister(username: string, password: string) {
    if (!this.isRegisterRequiredError()){
      this.userService.CreateUser({username: username, password: password, type: Role.JUNIOR}).subscribe((data: User) => {
          console.log({...data});
          console.log('register');
          this.registerFailed = false;
          
          this.fireAlertService.fireSuccess('User added successfully');
          // alert('register!');
          
          this.dialogRef.close({status: 200, userAdded: data});
        }, (error: HttpErrorResponse) => {
          console.log(error);
          if (error.status === 400) {
          console.log('Username already exists');
          this.registerFailed = true;

          this.fireAlertService.fireCreateError('Username');

          // alert('user already exists');
        }
      })
    }
  }

  onLeave() {
    this.dialogRef.close({ status: 201 })
  }

  // fireCreateError(msg: string) {
  //   Swal.fire({
  //     title: 'Input Error',
  //     icon: 'error',
  //     text: msg + ' is already exists'
  //   })
  // }

  // fireInputWarning(msg: string) {
  //   Swal.fire({
  //     title: 'Required field',
  //     icon: 'warning',
  //     text: msg 
  //   })
  // }

  // fireInputNotMatchWarning() {
  //   Swal.fire({
  //     title: 'Unmatched fields',
  //     icon: 'warning',
  //     text: 'Password and Confirm are not the same'
  //   })
  // }

  // fireSuccess(msg: string) {
  //   Swal.fire({
  //     title: msg,
  //     icon: 'success',
  //     showConfirmButton: false,
  //     timer: 1500,
  //     timerProgressBar: true,
  //     position: 'top',
  //   });
  // }

}
