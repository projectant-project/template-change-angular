import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UpdateResponse } from '../interfaces/update-response.interface';
import { User } from '../interfaces/user.interface';
import { ErrorStateMatcherService } from '../services/error-state-matcher.service';
import { FireAlertService } from '../services/fire-alert.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  user: User;
  myForm: FormGroup;
  matcher = new ErrorStateMatcherService();
  
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private fireAlertService: FireAlertService
  ) {
    this.myForm = this.formBuilder.group(
      {
        password: ["", [Validators.required]],
        confirmPassword: [""],
      },
      { validator: this.checkPasswords }
    );
  }

  ngOnInit(): void {
    this.user = this.userService.getUser();
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirm = group.controls.confirmPassword.value;
    return pass === confirm ? null : { notSame: true };
  }

  onSubmit(password: string) {
    if (!this.isRequiredError()) {
      let user: User = this.userService.getUser();
      this.userService
        .HttpUpdateOneUser(password, user.username)
        .subscribe((answer: UpdateResponse) => {
          console.log(answer);
          if (answer.ok === 0) {
            alert("error occured");
          } else {
            // alert('your password updated successfully');
            this.fireAlertService.fireSuccess(
              "your password updated successfully"
            );
          }

          this.userService.OnPasswordChange(user);
        });
    }
  }

  isRequiredError(): boolean {
    if (this.myForm.hasError("required", "password")) {
      // alert('Password field is required');
      this.fireAlertService.fireInputWarning("Password field is required");
      return true;
    } else if (this.myForm.hasError("notSame")) {
      // alert('Password and Confirm are not the same');
      this.fireAlertService.fireInputNotMatchWarning();
      return true;
    } else {
      return false;
    }
  }

}
