import { NgModule, Injectable } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  IconCamera,
  IconEdit3,
  IconUser,
  IconTrash2,
  IconEye,
  IconEyeOff,
  IconUserPlus,
  IconVideo,
  IconPlusCircle,
  IconSearch,
  IconMenu,
  IconHome,
} from "angular-feather";

const icons = [
  IconCamera,
  IconEdit3,
  IconUser,
  IconTrash2,
  IconEye,
  IconEyeOff,
  IconUserPlus,
  IconVideo,
  IconSearch,
  IconPlusCircle,
  IconMenu,
  IconHome,
];

@Injectable({
  providedIn: "root",
})
@NgModule({
  exports: icons,
  declarations: [],
  imports: [CommonModule],
})
export class IconsFeatherModule {}
