import { AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { start } from 'repl';
import { Subject } from 'rxjs';
import { SourceElement } from '../interfaces/sourceElement.interface';
import { TelemetryItem} from '../../interfaces/telemetry-item.interface';

@Component({
  selector: 'app-video-box',
  templateUrl: './video-box.component.html',
  styleUrls: ['./video-box.component.css']
})
export class VideoBoxComponent implements OnInit, AfterViewInit {

  @Input('sourceElement') sourceElm: SourceElement;
  @Input('idIndex') index: number;
  @Input('changeSize') changeSize: Subject<any>;
  @Output('removeItem') removeItem: EventEmitter<any> = new EventEmitter();
  @Output('checkItem') checkItem: EventEmitter<any> = new EventEmitter();
  source: SourceElement;
  videoJs;

  device: string;
  socket: WebSocket;
  telemetryList: TelemetryItem[] = [];
  telemetryDict = {};
  telemetryIndexer: [] = [];
  sensorsWhiteList: string[];
  sensorsBlackList: string[];
  top_arr: string[];
  bottom_arr: string[];
  waitingNotification: string;
  timeSpan = 0;
  currentIndex = 0;
  mutex = false;
  maxHeight: number
  maxWidth: number;
  sourceHeight: number;
  sourceWidth: number;
  videoContainer;
  canvasContainer;
  canvas;
  context;
  video;
  videoControls;
  playpause;
  mute;
  progress;
  progressBar;
  fullscreen;
  timer;
  volume;
  volumeBar;
  tries;
  prevVolume;
  squareInMiddlePart = 3;
  space;
  length;
  bottomAdapter;
  fontsize;
  panterFont;
  dataCounter;

  constructor() { }

  ngOnInit() {

    this.panterFont = "#5950e8";
    this.prevVolume = 0.5;
    this.tries = 0;
    this.source = this.sourceElm;
    this.device = this.source.title;

    this.sensorsWhiteList = ["ACCL", "GYRO", "SHUT", "ISOE", "ISOG", "GPS5"];
    this.top_arr  = ["ACCL", "GYRO", "SHUT"];
    this.bottom_arr = ["ISOE", "Lat.", "Long.", "Alt.", "2D speed"]

    this.telemetryDict = {
      "ACCL": '',
      "GYRO": '',
      "SHUT": '',
      "Lat." : '',
      "Long." : '',
      "Alt." : '',
      "2D speed" : '',
      "ISOE" : ''
    }
  
    this.waitingNotification = "Fetching data...";

    this.changeSize.subscribe(event => {
      
      console.log('child got the event')
      setTimeout(() => {
        this.setCanvasSize();
        // this.canvasContainer.remove(this.canvas);
        this.canvas.setAttribute('z-index', '1');
        console.log(this.canvas);
        // this.canvasContainer.appendChild(this.canvas);
        // this.setContextStyles();
        this.drawOnCanvas();
      }, 200)
    });
   
    console.log('video-box was init!!!!');
  }

  ngAfterViewInit() {

    this.socket = new WebSocket('ws://127.0.0.1:80');

    this.initListeners()
  }

  ngOnDestroy() {

    console.log("destroying: ", this);
    this.socket.close();
    this.videoJs.dispose();
    this.changeSize.observers.splice(this.index, 1);

  }

  initListeners(){

    this.videoContainer = document.getElementById('video-container-' + this.index.toString());
    this.canvasContainer = document.getElementById('canvas-container-' + this.index.toString())
    this.canvas = document.getElementById('telemetry-canvas-' + this.index.toString());
    this.context = this.canvas.getContext('2d');
    this.video = document.getElementById("video-unit-" + this.index.toString());

    this.playpause = document.getElementById('playpause-' + this.index.toString());
    this.mute = document.getElementById('mute-' + this.index.toString());
    this.progress = document.getElementById('progress-' + this.index.toString());
    this.progressBar = document.getElementById('progress-bar-' + this.index.toString());
    this.timer = document.getElementById('timer-' + this.index.toString());
    this.fullscreen = document.getElementById('fs-' + this.index.toString());
    this.volume = document.getElementById('volume-' + this.index.toString());
    this.volumeBar = document.getElementById('volume-bar-' + this.index.toString());

    this.playpause.addEventListener('click', () => {
      console.log('play', this)
      if (this.video.paused || this.video.ended) {
        this.video.play();
        this.playpause.innerHTML = "||";
      }
      else {
        this.video.pause();
        this.playpause.innerHTML = "▶";
      }
    });

    this.mute.addEventListener('click',() => {
      console.log(this.video.muted);
      this.video.muted = !this.video.muted;
      if (this.video.muted) {
        this.mute.innerHTML = '🔇';
        this.video.volume = 0;
        this.volume.value = 0;
      }
      else {
        this.mute.innerHTML = '🔊';
        this.video.volume = this.prevVolume;
        this.volume.value = this.prevVolume;
      }
    });

    this.video.addEventListener('load', () => {
      this.progress.setAttribute('max', this.video.duration);
    });

    this.video.addEventListener('ended', () => {
      this.playpause.innerHTML = "▶";
      this.currentIndex = 0;
    })

    this.video.addEventListener('timeupdate', () => {
        if (this.progress.getAttribute('max') === "NaN" || this.progress.getAttribute('max') === null) this.progress.setAttribute('max', this.video.duration);
        this.progress.value = this.video.currentTime;
        // this.progressBar.style.width = Math.floor((this.video.currentTime / this.video.duration) * 100) + '%';
        this.timer.innerHTML = `${Math.floor(this.video.currentTime)} / ${Math.floor(this.progress.getAttribute('max'))}`;
    });

    this.progress.addEventListener('click', (e:MouseEvent) => {
      console.log('progress');
      console.log('client x = ', e.x);
      let item: DOMRect = (this.progress as HTMLElement).getClientRects().item(0);
      console.log('progress bar found on x = ', Math.floor(item.x));
      
      var pos = (e.x  - Math.floor(item.x)) / item.width;
      console.log('pos');
      // var pos = (e.pageX  - this.progress.getClientRects()) / e.pageX;
      this.video.currentTime = pos * this.video.duration;
      this.binarysearch();
    });

    this.volume.addEventListener('click', (e:MouseEvent) => {
      console.log('volume');
      console.log('client x = ', e.x);
      let item: DOMRect = (this.volume as HTMLElement).getClientRects().item(0);
      console.log('volume bar found on x = ', Math.floor(item.x));
      
      var pos = (e.x  - Math.floor(item.x)) / item.width;
      console.log('pos', pos);

      this.prevVolume = pos;
      
      // if (this.prevVolume == 0 && pos > 0) this.mute.click();
      // else if (pos == 0 && this.prevVolume > 0) this.mute.click();
      
      if (this.video.muted){
        this.mute.click();
      }
      else {
        this.video.volume = pos;
        this.volume.value = pos;
      }
    });

    this.fullscreen.addEventListener('click', () => {
      if (!document.fullscreenElement){
          console.log('canvasContainer is requesting full screen', this.canvasContainer)
          this.canvasContainer.requestFullscreen();
          this.canvas.height = 950;
          this.canvas.width = 1750;
      }
      else{
          console.log('canvasContainer is requesting exit full screen')
          document.exitFullscreen();
          this.setCanvasSize();
      }

      this.setContextStyles();
      this.drawOnCanvas();
    });

    this.setCanvasSize()

    this.socket.onopen = () => this.handleOpen();
    this.socket.onerror = (msg) => this.handleError(msg);
    this.socket.onmessage = (msg) => this.handleMsg(msg);
    this.socket.onclose = () => this.handleClose();
  
  }

  handleOpen() {
    
    this.socket.send(this.device);

    this.setContextStyles();

    var videoSource = `http://localhost:7000/video/${this.device}/${this.device}.m3u8`;
    console.log(videoSource);

    //videoJS 
    this.videoJs = videojs(this.video, () => { });
    this.videoJs.src(videoSource);
    this.video.addEventListener('canplaythrough',
        () => {
            this.drawOnCanvas()
            setInterval(() => this.videoCanvas(), 30);
    })
  }

  // handleMsg(msg) {

  //   let json = JSON.parse(msg.data)
  //   let objectJson = json.datas;
  //   let sensorString = "";
  //   // console.log(this.sensorsWhiteList)

  //   if (this.timeSpan == 0) {
  //     this.timeSpan = json.timeSpan;
  //   }

  //   for (let i = 0; i < objectJson.length; i++) {

  //     if (this.sensorsWhiteList.includes(objectJson[i].key)) {

  //       if (objectJson[i].key == 'GPS5')
  //       {
  //         let datas = objectJson[i].data.split(', ');
  //         let name = objectJson[i].name;
  //         name = name.substring(
  //                 name.lastIndexOf("(") + 1, 
  //                 name.lastIndexOf(")")
  //                 );

  //         let comp = name.split(', ');
          
  //         const index = comp.indexOf("3D speed", 0);
  //         if (index > -1) {
  //           comp.splice(index, 1);
  //         }
  //         // console.log(datas, name, comp);

  //         for (let index = 0; index < comp.length; index++) {
  //           const element = datas[index]; 
  //           const key = comp[index];
  //           this.telemetryDict[key] = element;
  //         }
  //       }
  //       else
  //         this.telemetryDict[objectJson[i].key] = objectJson[i].data;
  //     }
  //   }
   
  //   for(let key in this.telemetryDict){
  //       let dataString = this.telemetryDict[key];
  //       let values = dataString.split(',')
  //       sensorString += `${key} --> `;
    
  //       for(let i = 0; i < values.length; i++)
  //       {
  //           if (i != 0)
  //           {
  //               if (this.isAlpha(values[i-1]))
  //                   sensorString += ':';
  //               else
  //                   sensorString += ',';
  //           }
    
  //           sensorString += values[i]
  //       }

  //       sensorString += '\n';
  //   }

  //   let item: TelemetryItem = {timeOffset: json.timeOffset, value: sensorString};
  //   // item.value = sensorString;
  //   // item.timeOffset = json.timeOffset;
  //   this.telemetryList.push(item)
  // }

  handleMsg(msg) {

    let json = JSON.parse(msg.data)
    let objectJson = json.datas;
    let sensorString = "";
    // console.log(this.sensorsWhiteList)

    if (this.timeSpan == 0) {
      this.timeSpan = json.timeSpan;
    }

    for (let i = 0; i < objectJson.length; i++) {

      if (this.sensorsWhiteList.includes(objectJson[i].key)) {

        if (objectJson[i].key == 'GPS5')
        {
          let datas = objectJson[i].data.split(', ');
          let name = objectJson[i].name;
          name = name.substring(
                  name.lastIndexOf("(") + 1, 
                  name.lastIndexOf(")")
                  );

          let comp = name.split(', ');
          
          const index = comp.indexOf("3D speed", 0);
          if (index > -1) {
            comp.splice(index, 1);
          }
          // console.log(datas, name, comp);

          for (let index = 0; index < comp.length; index++) {
            const element = datas[index]; 
            const key = comp[index];
            this.telemetryDict[key] = element;
          }
        }
        else if (objectJson[i].key == 'ISOG')
        this.telemetryDict["ISOE"] = objectJson[i].data
        else
          this.telemetryDict[objectJson[i].key] = objectJson[i].data;
      }
    }
   
    for(let key in this.telemetryDict){
        let dataString = this.telemetryDict[key];
        let values = dataString.split(',')
        sensorString += `${key}: `;
    
        for(let i = 0; i < values.length; i++)
        {
            if (i != 0)
            {
                if (this.isAlpha(values[i-1]))
                    sensorString += ':';
                else
                    sensorString += ' ';
            }
    
            sensorString += values[i]
        }

        sensorString += '\n';
    }

    let item: TelemetryItem = {timeOffset: json.timeOffset, value: sensorString};
    // item.value = sensorString;
    // item.timeOffset = json.timeOffset;
    this.telemetryList.push(item)
  }

  handleClose(){
    console.log('Socket closed !');
    this.socket.close();

  }

  handleError(msg){
    console.log('error', msg);
    this.waitingNotification = 'Error was detection. The source is already being process'
    this.socket.close();
  }

  removeVideo(event: MouseEvent) {
    let msg = {mEvent: event, value: this.source};
    this.removeItem.emit(msg);
  }

  checkVideo(event: MouseEvent) {
    let msg = {mEvent: event, value: this.source};
    this.checkItem.emit(msg);
  }

  isAlpha(str){
    return !/[^ a-zA-Z]/.test(str)
  }

  // calcTelemetryIndex(){
    
  //   if (this.currentIndex >= this.telemetryList.length - 2)
  //   {
  //     this.currentIndex = this.telemetryList.length - 1;
  //   }
  //   else{

  //     let next: number = (Number)(this.telemetryList[this.currentIndex + 1].timeOffset);
  //     let current: number = this.video.currentTime;
  // 
  //     // video offset is higher than telemetry offset
  //     if (current >= next)
  //     {
  //       this.currentIndex++;
  //     }
  //   }
    
  // }

  calcTelemetryIndex(){
    
    if (this.currentIndex >= this.telemetryList.length - 2)
    {
      this.currentIndex = this.telemetryList.length - 1;
    }
    else{

      let next: number = (Number)(this.telemetryList[this.currentIndex + 1].timeOffset);
      let current: number = this.video.currentTime;
      let after: number = (Number)(this.telemetryList[this.currentIndex + 2].timeOffset);

      // video offset is higher than telemetry offset
      if (current >= next && current < after)
      {
        this.currentIndex++;
      }
      else{
        this.binarysearch();
      }
    }
    
  }

  binarysearch(){
    let search: number = this.video.currentTime;
    let last = this.telemetryList.length;
    let first = 0;
    let middle;

    do
    {
      middle = Math.floor((last + first) / 2);
      if (search < (Number)(this.telemetryList[middle].timeOffset))
        last = middle - 1;
      else if (search > (Number)(this.telemetryList[middle].timeOffset))
        first = middle + 1;
    }while (search != (Number)(this.telemetryList[middle].timeOffset) && last >= first);

    if (search == (Number)(this.telemetryList[middle].timeOffset))
      this.currentIndex = middle;
    else{
      this.currentIndex = last - 1;
    }
  }

  setCanvasSize(){
    this.sourceHeight = this.videoContainer.clientHeight;
    this.sourceWidth = this.videoContainer.clientWidth;
    console.log('setCanvasSize!', this, `tries: ${this.tries}`)
    console.log('width', this.sourceWidth, 'height', this.sourceHeight);
    
    if ((this.sourceWidth === 0 || this.sourceHeight === 0) && this.tries < 2){

      setTimeout(() => {
        this.tries++;
        this.setCanvasSize()
      })
    }
    else if (this.tries === 2){
      console.log('-- WARNING: TRIES NUMBER 2 --')
      this.canvas.height = '100%';
      this.canvas.width = '100%';
      console.log(this.canvas);
    }
    else{
      this.canvas.height = this.sourceHeight;
      this.canvas.width = this.sourceWidth;
      console.log(this.canvas);
      this.tries = 0;
    }

    this.setContextStyles();
  }

  // setContextStyles(){
  //   // this.context.lineWidth = 10;
  //   let pixelsBox = Math.floor(this.canvas.height * this.canvas.width);
  //   let container = document.getElementById('gridster-container');
  //   let pixelsContainer = Math.floor(container.clientWidth * container.clientHeight);
  //   let ratio = pixelsBox / pixelsContainer;
  //   let fontSize = 7 + ratio * 5;
  //   this.fontsize = fontSize;
  //   this.context.lineWidth = 3 + ratio * 5;
  //   console.log(pixelsBox, container, pixelsContainer, ratio, fontSize);
  //   // let fontSize = resolution + ratioforSize;
  //   // console.log(this, resolution, ratioforSize, fontSize);

  //   // this.context.strokeStyle = "#00e013";
  //   // this.context.fillStyle = "#00e013";

  //   this.context.strokeStyle = this.panterFont;
  //   this.context.fillStyle = '#ffffff';
  //   this.context.font = `bold ${fontSize}px Arial`;

  //   // this.context.shadowColor = 'black';
  //   // this.context.shadowBlur = 5;
  //   // this.context.shadowOffsetX = 2;
  //   // this.context.shadowOffsetY = 2;

  //   if (this.canvas.height < 200 && this.canvas.height > 150)
  //     this.space = 10;
  //   else
  //     this.space = 15;
  // }

  setContextStyles(){

    let pixelsBox = Math.floor(this.canvas.height * this.canvas.width);
    let container = document.getElementById('gridster-container');
    let pixelsContainer = Math.floor(container.clientWidth * container.clientHeight);
    let ratio = pixelsBox / pixelsContainer;
    let fontSize = 8 + ratio * 5;
    this.fontsize = fontSize;
    this.context.lineWidth = 3 + ratio * 5;
    console.log(pixelsBox, container, pixelsContainer, ratio, fontSize);

    this.context.strokeStyle = this.panterFont;
    this.context.fillStyle = '#ffffff';
    this.context.font = `bold ${fontSize}px Arial`;

    if (this.canvas.height < 200 && this.canvas.height > 150)
      this.space = 10;   
    else
      this.space = 17.5;

    this.length = this.canvas.width / this.bottom_arr.length;
  }

  videoCanvas() {   
      if (!this.video.ended && !this.video.paused) {
        this.drawOnCanvas()
    }
  }

  // drawOnCanvas() {


  //   this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
  //   this.context.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height)
    
  //   if(this.telemetryList.length > 0)
  //   {
  //     if (this.canvas.width > 400 || this.canvas.height > 200)
  //       this.dataCounter = 0;
  //       this.drawFocusLines();

  //       this.calcTelemetryIndex();
  //       console.log(this.telemetryList, this.currentIndex);
  //       let sensorsStr = this.telemetryList[this.currentIndex].value;
  //       let sensorsArr = sensorsStr.split('\n');
  //       console.log(sensorsArr)


  //       // let topLeft = this.topLeftConfig();
  //       // let topRight = this.topRightConfig();
  //       // let bottomRight = this.bottomRightConfig();
  //       let bottomLeft = this.bottomLeftConfig();
        
  //       for (let i = 0; i < sensorsArr.length; i++) {

  //           let place = sensorsArr[i].substring(0, 4);

  //           this.drawLine(sensorsArr[i], bottomLeft)
            
  //           if (this.top_arr.includes(place))
  //             this.dataCounter++;
            
  //           bottomLeft[0] += this.length;
            
  //           if (this.top_arr.length === this.dataCounter)
  //           {
  //             this.dataCounter = 0;
  //             bottomLeft = this.bottomLeftConfig();
  //             bottomLeft[1] += this.space
  //           }
  //       }
  //   }
  //   else{
  //     this.context.fillText(this.waitingNotification, 5, 70);
  //   }
  // }

  drawOnCanvas() {


    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.context.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height)
    
    if(this.telemetryList.length > 0)
    {
      if (this.canvas.width > 400 || this.canvas.height > 200)
        this.dataCounter = 0;
        this.drawFocusLines();

        this.calcTelemetryIndex();
        console.log(this.telemetryList, this.currentIndex);
        let sensorsStr = this.telemetryList[this.currentIndex].value;
        let sensorsArr = sensorsStr.split('\n');
        console.log(sensorsArr)

        let bottomLeft = this.bottomLeftConfig();
        
        for (let i = 0; i < sensorsArr.length; i++) {

            if (sensorsArr[i] !== "")
            {
              let place = sensorsArr[i].substring(0, 4);
              
              if (this.top_arr.includes(place))
              {
                this.bottomAdapter = this.length;
                this.drawLine(sensorsArr[i], bottomLeft)
                this.dataCounter++;
  
              }
              else{
                this.bottomAdapter = 0;
                if (this.dataCounter === this.top_arr.length)
                {
                  bottomLeft = this.bottomLeftConfig();
                  this.dataCounter = 0;
                }
  
                this.drawSpecial(sensorsArr[i], bottomLeft);
              }
              
              bottomLeft[0] += this.length + this.bottomAdapter;
            }
          
        }
    }
    else{
      this.context.fillText(this.waitingNotification, 5, 70);
    }
  }

  drawSpecial(sensorData, configList){

    let arr = sensorData.split(': ');
    let sensorString = arr[1];
    let sensorKey = arr[0];

    if (sensorString === '')
      sensorString = '-'

    console.log("-- VALUE:", sensorData)
    console.log('-- FILLING TEXT:', sensorString, ' on x: ', configList[0], ' y: ', configList[1]);

    this.context.font = `bold ${this.fontsize + 6}px Arial`;
    this.context.fillText(sensorString, configList[0], configList[1]);
    this.context.font = `bold ${this.fontsize}px Arial`;
    this.context.fillText(sensorKey, configList[0], configList[1] + 0.75 * this.space);
  }

  drawLine(sensorData, configList){

    console.log("-- VALUE:", sensorData)
    console.log('-- FILLING TEXT:', sensorData, ' on x: ', configList[0], ' y: ', configList[1]);

    this.context.fillText(sensorData, configList[0], configList[1] + 2.25 * this.space);
  }

  drawFocusLines(){
    let thirdHeight = this.canvas.height / this.squareInMiddlePart, thirdWidth = this.canvas.width / this.squareInMiddlePart;
    let heightLength = thirdHeight / 4, widthLength = thirdWidth / 4; 
    let widthStart, heightStart;
    let topPadding = -20;
    
    // if (this.canvas.height < 400 && this.canvas.height > 350) {
    //   topPadding = 20; 
    // }

    // top left
    widthStart = thirdWidth, heightStart = thirdHeight + topPadding;
    this.context.moveTo(widthStart + widthLength, heightStart);
    this.context.lineTo(widthStart, heightStart);
    this.context.lineTo(widthStart, heightStart + heightLength);

    // top right
    widthStart = (this.squareInMiddlePart - 1) * thirdWidth, heightStart = thirdHeight + topPadding;
    this.context.moveTo(widthStart - widthLength, heightStart);
    this.context.lineTo(widthStart, heightStart);
    this.context.lineTo(widthStart, heightStart + heightLength);

    // bottom left
    widthStart = thirdWidth, heightStart = (this.squareInMiddlePart - 1) * thirdHeight + topPadding;
    this.context.moveTo(widthStart, heightStart - heightLength);
    this.context.lineTo(widthStart, heightStart);
    this.context.lineTo(widthStart + widthLength, heightStart);

    // bottom right
    widthStart = (this.squareInMiddlePart - 1) * thirdWidth, heightStart = (this.squareInMiddlePart - 1) * thirdHeight + topPadding;
    this.context.moveTo(widthStart, heightStart - heightLength);
    this.context.lineTo(widthStart, heightStart);
    this.context.lineTo(widthStart - widthLength, heightStart);

    this.context.stroke();
  }

  bottomLeftConfig() { 
    console.log(this.fontsize)
    // let height = this.fontsize * 3.5;
    let height = this.space * 3;
    return [10, this.canvas.height - 20 - height]
    // return [10, this.canvas.height - 30 - height]
  }
}
