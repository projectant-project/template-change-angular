import { SourceElement } from "./sourceElement.interface";

export class VideoElement {
    source: SourceElement;
    videoElement: HTMLVideoElement;
}