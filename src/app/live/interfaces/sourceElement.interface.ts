
export class SourceElement {
    title: string;
    description: string;
    uri: string;
}