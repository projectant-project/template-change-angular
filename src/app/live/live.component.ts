import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  AfterViewChecked,
  AfterContentInit,
  Input,
} from "@angular/core";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDrag,
  CdkDragEnd,
} from "@angular/cdk/drag-drop";
import { VideoElement } from "./interfaces/videoElement.interface";
import { SourceElement } from "./interfaces/sourceElement.interface";
import {
  GridsterConfig,
  GridsterItem,
  GridsterItemComponent,
  GridType,
  CompactType,
  GridsterPush,
} from "angular-gridster2";
import { subscribeOn } from "rxjs/operators";
import { EventEmitter } from "protractor";
import { Subject } from "rxjs";
import { Source } from '../interfaces/source.interface';
import { SourceService } from '../services/source.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css']
})
export class LiveComponent implements OnInit {
  options: GridsterConfig;
  itemToPush: GridsterItemComponent;
  dashboard: Array<GridsterItem>;
  sourceToPush: SourceElement;
  changeSizes: Subject<any>;

  sources: Source[] = [];
  videoSource = "http://localhost:7000/video/";
  sourcesElmList: SourceElement[] = [];
  sourceDisplayList: SourceElement[] = [];

  constructor(private sourceService: SourceService) {}

  itemChange(item, itemComponent) {
    console.info("item changed", item, itemComponent);
    console.log("item changed !!!", item, itemComponent);
  }

  itemResize(item, itemComponent) {
    console.info("item resized", item, itemComponent);
    console.log("item resized !!!", item, itemComponent);
  }

  ngOnInit() {
    if (this.sourceService.sources.length === 0)
    {
      this.sourceService.HttpGetSources().subscribe((sources: Source[]) => {
        console.log(sources);

        sources = sources.sort((a, b) => (a.title > b.title) ? 1 : -1);
        this.sources = sources;
        this.sourceService.setSources(sources);
      });
    }
    else{
      this.sources = this.sourceService.getSources(); 
      console.log(this.sources);
    }
    
    console.log("SOURCES !!", this.sources);
    for (let i = 0; i < this.sources.length; i++) {

      // let device = this.devices[i % this.devices.length];
      // let url = `${this.videoSource}${device}/${device}.m3u8`;
     let device = this.sources[i].title;
     let url = `${this.videoSource}${device}/${device}.m3u8`;

      var sourceElm: SourceElement = {
        title: `${this.sources[i].title}`,
        description: "No description",
        uri: url,
      };
      this.sourcesElmList.push(sourceElm);
    }

    this.options = {
      gridType: GridType.Fit,
      compactType: CompactType.None,
      pushItems: true,
      draggable: { enabled: true, stop: (e, ui, $widget) => {
        console.log('item stop dragging.');
        setTimeout(() => this.changeSizes.next('changed'), 100);
        // console.log('change emitted');
      }},
      resizable: { enabled: true, stop: (e, ui, $widget) => {
        console.log('item stop resizing.');
        setTimeout(() => this.changeSizes.next('changed'), 100);
        // console.log('change emitted');
      }},
      maxCols: 3,
      maxRows: 3,
    };

    console.log(this.sourcesElmList);

    this.dashboard = [];
    this.changeSizes = new Subject<any>();
  }

  changedOptions() {
    console.log("changed options");
    if (this.options.api && this.options.api.optionsChanged)
      this.options.api.optionsChanged();
  }

  removeItem(msg: { event: MouseEvent; value: SourceElement }) {
    console.log(msg);
    let index = this.sourceDisplayList.indexOf(msg.value);
    console.log(
      "index: ",
      index,
      " title: ",
      this.sourceDisplayList[index].title
    );
    console.log(this.dashboard[index]);

    console.log(this.sourceDisplayList);
    console.log(this.dashboard);

    let source: SourceElement = this.sourceDisplayList.splice(index, 1)[0];
    console.log("pushing to source list:", source);
    this.sourcesElmList.push(source);

    event.preventDefault();
    event.stopPropagation();
    this.dashboard.splice(index, 1);

    this.updateVideosID(index);

    setTimeout(() => this.changeSizes.next('changed'), 300);
    console.log('item removed')
  }

  updateVideosID(index: number) {
    console.log(index);
    console.log(this.sourceDisplayList);
    let str = "video-unit-";
    for (let i = index; i < this.sourceDisplayList.length; i++) {
      console.log(i);
      console.log(this.sourceDisplayList[i]);
      let element = document.getElementById(str + (i + 1).toString());
      console.log(element);
      element.id = str + i.toString();
    }
  }

  removeAllItems() {
    console.log("deleting all array");
    let sources: SourceElement[] = this.sourceDisplayList.splice(
      0,
      this.sourceDisplayList.length
    );
    sources.forEach((source: SourceElement) =>
      this.sourcesElmList.push(source)
    );

    this.dashboard.splice(0, this.dashboard.length);
  }

  addItem() {
    console.log("add Item", this.sourceToPush);

    this.dashboard.push({ cols: 1, rows: 1, y: 0, x: 0 });
    console.log(this.dashboard);

    // setTimeout(() => this.changeSizes.next('changed'), 100);
  }

  addVideo() {
    console.log("video component loaded !!!");
    // this.sourceDisplayList.push(this.sourceToPush);
  }

  initItem(item: GridsterItem, itemComponent: GridsterItemComponent) {
    console.log("init Item");
    this.itemToPush = itemComponent;
  }

  drop(event: CdkDragDrop<SourceElement[]>) {
    // if the pointer is outside the container
    console.log('dropped', event);
    if (!event.isPointerOverContainer) {
      let element = document.getElementsByClassName("video-container")[0];
      let bound = element.getBoundingClientRect();

      const e: MouseEvent = window.event as MouseEvent;
      const position = { x: e.clientX, y: e.clientY };

      // x and y are inside bound
      if (
        position.x > bound.left &&
        position.x < bound.right &&
        position.y > bound.top &&
        position.y < bound.bottom
      ) {
        this.sourceToPush = this.sourcesElmList.splice(
          event.previousIndex,
          1
        )[0];
        this.sourceDisplayList.push(this.sourceToPush);
        console.log("pushing to video list:", this.sourceToPush);
        this.addItem();
      }
      console.log('item dropped')
      setTimeout(() => this.changeSizes.next('changed'), 100);
    }
  }
}