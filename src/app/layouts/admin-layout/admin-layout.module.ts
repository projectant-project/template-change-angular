import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { UserProfileComponent } from '../../user-profile/user-profile.component';
// import { TableListComponent } from '../../table-list/table-list.component';
// import { TypographyComponent } from '../../typography/typography.component';
// import { MapsComponent } from '../../maps/maps.component';
// import { NotificationsComponent } from '../../notifications/notifications.component';
// import { UpgradeComponent } from '../../upgrade/upgrade.component';

import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { IconsComponent} from '../../icons/icons.component'

import { HomeComponent } from '../../home/home.component';
import { UserManagementComponent } from '../../user-management/user-management.component';
import { SourceManagementComponent } from '../../source-management/source-management.component';
import { StatisticsComponent } from '../../statistics/statistics.component';
import { LiveComponent } from '../../live/live.component';
import { VideoBoxComponent } from '../../live/video-box/video-box.component';

import { PageUnauthorizedComponent } from '../../page-unauthorized/page-unauthorized.component';

import { IconsFeatherModule } from '../../icons-feather/icons.module';

import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatGridListModule } from "@angular/material/grid-list";
import { GridsterModule} from 'angular-gridster2';
import { MatCardModule } from "@angular/material/card";
import { MatSelectModule } from '@angular/material/select'

import { DeleteSourceDialogComponent } from '../../source-management/dialogs/delete-source-dialog/delete-source-dialog.component';
import { DeleteAllSourcesDialogComponent } from '../../source-management/dialogs/delete-all-sources-dialog/delete-all-sources-dialog.component';
import { AddSourceDialogComponent } from '../../source-management/dialogs/add-source-dialog/add-source-dialog.component';
import { DeleteUserDialogComponent } from "../../user-management/dialogs/delete-user-dialog/delete-user-dialog.component";
import { DeleteAllUsersDialogComponent } from "../../user-management/dialogs/delete-all-users-dialog/delete-all-users-dialog.component";
import { AddUserDialogComponent } from "../../user-management/dialogs/add-user-dialog/add-user-dialog.component";


@NgModule({
  declarations: [
    DashboardComponent,
    IconsComponent,
    LiveComponent,
    VideoBoxComponent,
    HomeComponent,
    StatisticsComponent,
    // PageUnauthorizedComponent,

    UserManagementComponent,
    SourceManagementComponent,
    

    DeleteUserDialogComponent,
    DeleteAllUsersDialogComponent,
    AddUserDialogComponent,
    DeleteSourceDialogComponent,
    DeleteAllSourcesDialogComponent,
    AddSourceDialogComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    
    IconsFeatherModule,
    
    MatListModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    DragDropModule,
    MatGridListModule,
    MatCardModule,
    GridsterModule,

    // DashboardComponent,
    // IconsComponent,
    // LiveComponent,
    // VideoBoxComponent,
    // HomeComponent,
    // StatisticsComponent,
    // PageUnauthorizedComponent,

    RouterModule.forChild(AdminLayoutRoutes),
    ToastrModule.forRoot()
  ],
  providers: [],
  entryComponents: [
    DeleteUserDialogComponent,
    DeleteAllUsersDialogComponent,
    AddUserDialogComponent,
    DeleteSourceDialogComponent,
    DeleteAllSourcesDialogComponent,
    AddSourceDialogComponent,
  ],
})

export class AdminLayoutModule {}
