import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { UserProfileComponent } from '../../user-profile/user-profile.component';
// import { TableListComponent } from '../../table-list/table-list.component';
// import { TypographyComponent } from '../../typography/typography.component';
// import { MapsComponent } from '../../maps/maps.component';
// import { NotificationsComponent } from '../../notifications/notifications.component';
// import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { LiveComponent } from '../../live/live.component';
import { HomeComponent } from '../../home/home.component';
import { UserManagementComponent } from '../../user-management/user-management.component';
import { SourceManagementComponent } from '../../source-management/source-management.component';
import { IconsComponent } from '../../icons/icons.component';
import { StatisticsComponent } from '../../statistics/statistics.component';
// import { PageUnauthorizedComponent } from '../../page-unauthorized/page-unauthorized.component';

export const AdminLayoutRoutes: Routes = [
    // { path: 'user-profile',         component: UserProfileComponent },
    // { path: 'table-list',           component: TableListComponent },
    // { path: 'typography',           component: TypographyComponent },
    // { path: 'maps',                 component: MapsComponent },
    // { path: 'notifications',        component: NotificationsComponent },
    // { path: 'upgrade',              component: UpgradeComponent }
    
    { path: 'dashboard',            component: DashboardComponent },
    { path: 'icons',                component: IconsComponent },
    { path: 'live',                 component: LiveComponent },
    { path: 'home',                 component: HomeComponent },
    { path: 'user-management',      component: UserManagementComponent },
    { path: 'source-management',    component: SourceManagementComponent },
    { path: 'statistics',           component: StatisticsComponent },
    // { path: 'unauthorized',         component: PageUnauthorizedComponent },
        
];
