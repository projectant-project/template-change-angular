import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

import { GuestLayoutRoutes } from './guest-layout.routing';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { IconsComponent} from '../../icons/icons.component'
import { HomeComponent } from '../../home/home.component';
import { StatisticsComponent } from '../../statistics/statistics.component';
import { LiveComponent } from '../../live/live.component';
import { VideoBoxComponent } from '../../live/video-box/video-box.component';
import { UserEditComponent } from '../../user-edit/user-edit.component';

// import { PageUnauthorizedComponent } from '../../page-unauthorized/page-unauthorized.component';

import { IconsFeatherModule } from '../../icons-feather/icons.module';

import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatGridListModule } from "@angular/material/grid-list";
import { GridsterModule} from 'angular-gridster2';
import { MatCardModule } from "@angular/material/card";
import { MatSelectModule } from '@angular/material/select'


@NgModule({
  declarations: [
    DashboardComponent,
    IconsComponent,
    LiveComponent,
    VideoBoxComponent,
    HomeComponent,
    StatisticsComponent,
    // PageUnauthorizedComponent,

    UserEditComponent

  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    
    IconsFeatherModule,
    
    MatListModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    DragDropModule,
    MatGridListModule,
    MatCardModule,
    GridsterModule,

    // DashboardComponent,
    // IconsComponent,
    // LiveComponent,
    // VideoBoxComponent,
    // HomeComponent,
    // StatisticsComponent,
    // PageUnauthorizedComponent,

    RouterModule.forChild(GuestLayoutRoutes),
    ToastrModule.forRoot()
  ],
  providers: [],
})
export class GuestLayoutModule { }
