import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserEditComponent } from '../../user-edit/user-edit.component';
import { LiveComponent } from '../../live/live.component';
import { HomeComponent } from '../../home/home.component';
import { IconsComponent } from '../../icons/icons.component';
import { StatisticsComponent } from '../../statistics/statistics.component';
// import { PageUnauthorizedComponent } from '../../page-unauthorized/page-unauthorized.component';

export const GuestLayoutRoutes: Routes = [
    { path: 'dashboard',            component: DashboardComponent },
    { path: 'icons',                component: IconsComponent },
    { path: 'live',                 component: LiveComponent },
    { path: 'home',                 component: HomeComponent },
    { path: 'profile-edit',         component: UserEditComponent },
    { path: 'statistics',           component: StatisticsComponent },
    // { path: 'unauthorized',         component: PageUnauthorizedComponent },
];