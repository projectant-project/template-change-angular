import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { from } from 'rxjs';

import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";

import { GridsterModule} from 'angular-gridster2';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { GuestLayoutComponent } from './layouts/guest-layout/guest-layout.component';
import { LoginComponent } from './login/login.component';

// import { UserManagementComponent } from './user-management/user-management.component';
// import { SourceManagementComponent } from './source-management/source-management.component';
import { LiveComponent } from './live/live.component';
import { VideoBoxComponent } from './live/video-box/video-box.component';
import { HomeComponent } from './home/home.component';
import { IconsComponent } from './icons/icons.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { PageUnauthorizedComponent } from './page-unauthorized/page-unauthorized.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,

    GridsterModule,

    MatFormFieldModule,
    MatInputModule, 

    ToastrModule.forRoot()
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    GuestLayoutComponent,
    LoginComponent,

    // DashboardComponent,
    // IconsComponent,
    // LiveComponent,
    // VideoBoxComponent,
    // HomeComponent,
    // StatisticsComponent,

    PageUnauthorizedComponent,
    PageNotFoundComponent,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
