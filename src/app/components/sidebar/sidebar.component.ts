import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const guestRoutes: RouteInfo[] = [
  { path: 'home', title: 'Home',  icon:'travel_info', class: '' },
  { path: 'icons', title: 'Icons',  icon:'education_atom', class: '' },
  { path: 'profile-edit', title: 'Profile Edit',  icon:'users_single-02', class: '' },
  { path: 'live', title: 'Live Stream',  icon:'media-1_button-play', class: '' },
  { path: 'statistics', title: 'Statistics',  icon: 'business_chart-bar-32', class: '' },
  { path: 'dashboard', title: 'Dashboard',  icon: 'design_app', class: '' },  
];

export const adminRoutes: RouteInfo[] = [
  { path: 'home', title: 'Home',  icon:'travel_info', class: '' },
  { path: 'icons', title: 'Icons',  icon:'education_atom', class: '' },
  { path: 'user-management', title: 'User Management',  icon:'users_single-02', class: '' },
  { path: 'source-management', title: 'Source Management',  icon:'media-1_camera-compact', class: '' },
  { path: 'live', title: 'Live Stream',  icon:'media-1_button-play', class: '' },
  { path: 'statistics', title: 'Statistics',  icon: 'business_chart-bar-32', class: '' },
  { path: 'dashboard', title: 'Dashboard',  icon: 'design_app', class: '' },
  
  
  // { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
  // { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
  // { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' },
  // { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
  // { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },
  // { path: '/upgrade', title: 'Upgrade to PRO',  icon:'objects_spaceship', class: 'active active-pro' }

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  guestItems: any[];
  adminItems: any[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    // this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.guestItems = guestRoutes.filter(menuItem => menuItem);
    this.adminItems = adminRoutes.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };

  isAdmin(){
    return this.userService.isAdmin();
  }
}
