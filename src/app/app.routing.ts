import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { GuestLayoutComponent } from './layouts/guest-layout/guest-layout.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardAdminService } from './services/auth-guard-admin.service';
import { AuthGuardGuestService } from './services/auth-guard-guest.service';
import { PageUnauthorizedComponent } from './page-unauthorized/page-unauthorized.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes =[
  {
    path: '',
    component: LoginComponent
  },
  { 
    path: 'unauthorized',
    component: PageUnauthorizedComponent
  },
  { 
    path: 'not-found',
    component: PageNotFoundComponent
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    canActivate: [AuthGuardAdminService],
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
    }],
  },
  {
    path: 'guest',
    component: GuestLayoutComponent,
    canActivate: [AuthGuardGuestService],
    children: [
        {
      path: '',
      loadChildren: './layouts/guest-layout/guest-layout.module#GuestLayoutModule'
    }],
  },
  { 
    path: '**',
    redirectTo: 'not-found'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
