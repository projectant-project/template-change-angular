export interface Source{
    uri: string,
    title: string,
    description: string
}