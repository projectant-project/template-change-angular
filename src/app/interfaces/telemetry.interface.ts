export interface Telemetry{
    device: string,
    key: string,
    name: string,
    value: string,
    time: string
}