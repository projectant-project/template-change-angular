export interface TelemetryItem{
    timeOffset: string,
    value: string,
}