export interface UpdateResponse{
    n: number;
    nModified: number;
    ok: number;
}