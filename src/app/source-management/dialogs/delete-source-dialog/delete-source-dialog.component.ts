import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-source-dialog',
  templateUrl: './delete-source-dialog.component.html',
  styleUrls: ['./delete-source-dialog.component.css']
})
export class DeleteSourceDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteSourceDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {uri: string, title: string, description: string}) { }

  ngOnInit() {
  }

}
