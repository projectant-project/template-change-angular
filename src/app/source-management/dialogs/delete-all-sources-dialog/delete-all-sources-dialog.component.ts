import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-all-sources-dialog',
  templateUrl: './delete-all-sources-dialog.component.html',
  styleUrls: ['./delete-all-sources-dialog.component.css']
})
export class DeleteAllSourcesDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteAllSourcesDialogComponent>) { }

  ngOnInit() {
  }

}
