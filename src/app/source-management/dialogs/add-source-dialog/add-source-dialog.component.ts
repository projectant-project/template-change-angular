import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Source } from '../../../interfaces/source.interface';
import { FireAlertService } from '../../../services/fire-alert.service';
import { SourceService } from '../../../services/source.service';

@Component({
  selector: 'app-add-source-dialog',
  templateUrl: './add-source-dialog.component.html',
  styleUrls: ['./add-source-dialog.component.css']
})
export class AddSourceDialogComponent implements OnInit {

  myForm: FormGroup;
  registerFailed = false;

  constructor(public dialogRef: MatDialogRef<AddSourceDialogComponent>, private sourceService: SourceService, private formBuilder: FormBuilder, private fireAlertService: FireAlertService) {
    this.myForm = this.formBuilder.group({
      uri: ['', [Validators.required]],
      title: ['', [Validators.required]],
      description: ['', [Validators.required]]});
  }

  ngOnInit() {
  }

  isRegisterRequiredError(): boolean {
    if (this.myForm.hasError('required', 'uri')){
      this.fireAlertService.fireInputWarning('Uri');
      return true; 
    }
    if (this.myForm.hasError('required', 'title')){
      this.fireAlertService.fireInputWarning('Title')
      return true; 
    }
    if (this.myForm.hasError('required', 'description')){
      this.fireAlertService.fireInputWarning('Description');
      return true; 
    }
    else{
      return false;
    }
  }
  
  onRegister(uri: string, title: string, description: string) {
    if (!this.isRegisterRequiredError()){
      this.sourceService.HttpCreateSource({uri, title, description}).subscribe((data: Source) => {
          console.log({...data});
          console.log('register');
          this.registerFailed = false;
          
          this.fireAlertService.fireSuccess('Source added successfully');
          // alert('register!');
          

          this.dialogRef.close({status: 200, sourceAdded: data});
        }, (error: HttpErrorResponse) => {
          console.log(error);
          if (error.status === 400) {
          console.log('Uri already exists');
          this.registerFailed = true;

          this.fireAlertService.fireCreateError('Uri');
          // alert('source already exists');
        }
      })
    }
  }

  onLeave() {
    this.dialogRef.close({ status: 201 })
  }

  // fireDbError() {
  //   Swal.fire({
  //     title: 'Error Occured',
  //     icon: 'error',
  //     text: 'Error occured while trying to update your password in database' 
  //   })
  // }

  // fireInputWarning(msg: string) {
  //   Swal.fire({
  //     title: 'Required field',
  //     icon: 'warning',
  //     text: msg 
  //   })
  // }

  // fireSuccess(msg: string) {
  //   Swal.fire({
  //     title: msg,
  //     icon: 'success',
  //     showConfirmButton: false,
  //     timer: 1500,
  //     timerProgressBar: true,
  //     position: 'top',
  //   });
  // }

}
