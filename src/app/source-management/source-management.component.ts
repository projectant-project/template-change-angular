import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Source } from "../interfaces/source.interface";
import { MatSelectionList, MatListOption } from "@angular/material/list";
import { MatDialog } from "@angular/material/dialog";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { UpdateResponse } from "../interfaces/update-response.interface";
import { SelectOperation } from "../enums/selected.enum";
import { startWith, debounceTime, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import { isNullOrUndefined } from "util";
import { AddSourceDialogComponent } from "./dialogs/add-source-dialog/add-source-dialog.component";
import { DeleteAllSourcesDialogComponent } from "./dialogs/delete-all-sources-dialog/delete-all-sources-dialog.component";
import { DeleteSourceDialogComponent } from "./dialogs/delete-source-dialog/delete-source-dialog.component";
import { FireAlertService } from "../services/fire-alert.service";
import { SourceService } from "../services/source.service";

class SourceSelect {
  source: Source;
  selected: boolean;

  constructor(source: Source, selected: boolean) {
    this.source = source;
    this.selected = selected;
  }
}

@Component({
  selector: 'app-source-management',
  templateUrl: './source-management.component.html',
  styleUrls: ['./source-management.component.css']
})
export class SourceManagementComponent implements OnInit {
  @ViewChild("checkRef", { static: true }) checkRef;
  status: string = "Select All";
  sources: SourceSelect[] = [];
  myForm: FormGroup;
  selectOperation: SelectOperation = SelectOperation.SELECT;
  checkSign = false;
  indeterminateSign = false;

  // serach
  tempSources: SourceSelect[] = [];
  sourceControl = new FormControl();
  search = new FormControl();

  $search = this.search.valueChanges.pipe(
    startWith(null),
    debounceTime(200),
    switchMap((res: string) => {
      if (!res) return of(this.sources);

      res = res.toLowerCase();
      return of(
        this.sources.filter((x) => x.source.title.toLowerCase().indexOf(res) >= 0)
      );
    })
  );

  constructor(
    private sourceService: SourceService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private fireAlertService: FireAlertService
  ) {
    this.myForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      description: ["", [Validators.required]],
    });
  }

  ngOnInit() {

    if (this.sourceService.sources.length === 0)
    {
      this.sourceService.HttpGetSources().subscribe((sources: Source[]) => {
        console.log(sources);

        sources = sources.sort((a, b) => (a.title > b.title) ? 1 : -1);

        sources.map((source: Source) => {
          this.sources.push(new SourceSelect(source, false));
        });

        this.sourceService.setSources(sources);
      });
    }
    else{
      let sources: Source[] = this.sourceService.getSources(); 
      console.log(sources);
      sources.map((source: Source) => {
        this.sources.push(new SourceSelect(source, false));
      });
    }
    
    this.$search.subscribe((sources: SourceSelect[]) => {
      console.log(this.tempSources, sources);
      if (!isNullOrUndefined(this.tempSources)) {
        if (!this.checkEqual(this.tempSources, sources)) {
          console.log("list changed !!");
          this.tempSources = sources;
          this.checkBoxState();
        }
      } else this.tempSources = sources;
    });
  }

  checkEqual(a1: any[], a2: any[]) {
    if (isNullOrUndefined(a1) || isNullOrUndefined(a2)) return false;

    if (a1.length !== a2.length) return false;

    for (let i = 0; i < a2.length; i++) {
      if (!a1.includes(a2[i])) return false;
    }

    console.log("somehow true");
    return true;
  }

  checkContain(a1: any[], a2: any[]) {
    // check if a1 contains a2
    if (isNullOrUndefined(a1) || isNullOrUndefined(a2)) return false;

    for (let i = 0; i < a2.length; i++) {
      if (!a1.includes(a2[i])) return false;
    }

    console.log("a1 contains a2");
    return true;
  }

  checkContainSome(a1: any[], a2: any[]) {
    // check if a1 contains some of a2
    if (isNullOrUndefined(a1) || isNullOrUndefined(a2)) return false;

    let counter: number = 0;
    for (let i = 0; i < a2.length; i++) {
      if (a1.includes(a2[i])) counter++;
    }

    return counter;
  }

  selectionChange(option: any) {
    console.log("selectionChange");
    console.log(option.value);

    console.log("temp sources: ", this.tempSources);
    console.log("before", this.sourceControl.value);

    let value = this.sourceControl.value || [];
    option.value.selected = option.selected;
    if (option.selected) value.push(option.value);
    else value = value.filter((x: any) => x != option.value);

    // adding to source control
    this.sourceControl.setValue(value);
    console.log("after", this.sourceControl.value);

    // changing source selected state
    let index: number = this.sources.findIndex(
      (x) => x.source.title === option.value.source.title
    );
    this.sources[index].selected = option.selected;

    // check box state now
    this.checkBoxState();
  }

  checkBoxState() {
    this.checkAllChecked();
    if (!this.checkSign) {
      this.checkIndeterminate();
    }
  }

  changeToSelectMode() {
    this.status = "Select All";
    this.selectOperation = SelectOperation.SELECT;
  }

  changeToDeselectMode() {
    this.status = "Deselect All";
    this.selectOperation = SelectOperation.DESELECT;
  }

  checkAllChecked() {
    if (!isNullOrUndefined(this.sourceControl.value)) {
      if (this.checkContain(this.sourceControl.value, this.tempSources)) {
        console.log(
          "all checked. changing status to deselect + operation to deselect + sign to v"
        );
        this.checkSign = true;
        this.indeterminateSign = false;
        this.changeToDeselectMode();
      } else {
        console.log(
          "not all selected. changing status to select + operation to select"
        );
        this.checkSign = false;
        this.changeToSelectMode();
      }
    }
  }

  checkIndeterminate() {
    if (
      isNullOrUndefined(this.sourceControl.value) ||
      this.sourceControl.value.length === 0
    ) {
      console.log("none checked.");
      this.indeterminateSign = false;
    } else {
      if (!this.checkContainSome(this.sourceControl.value, this.tempSources)) {
        console.log("no one of the searched users are checked.");
        this.indeterminateSign = false;
      } else {
        console.log("more than one of the searched users are checked.");
        this.indeterminateSign = true;
      }
    }
  }

  allCheckedOperation() {
    console.log("all checked operation");
    this.changeAllState();
    this.updateAll();
    this.checkAllChecked();
  }

  changeAllState() {
    console.log("changeAllState");
    this.tempSources.forEach((source: SourceSelect) => {
      let index: number = this.sources.findIndex(
        (x) => x.source.title === source.source.title
      );
      this.sources[index].selected =
        this.selectOperation === SelectOperation.SELECT ? true : false;
      source.selected =
        this.selectOperation === SelectOperation.SELECT ? true : false;
      console.log(source);
    });
  }

  updateAll() {
    let value = this.sourceControl.value || [];
    console.log("selected all is:", this.selectOperation);
    if (this.selectOperation === SelectOperation.SELECT) {
      console.log("inserting all temp into source control");
      this.tempSources.forEach((source: SourceSelect) => {
        if (!value.includes(source)) value.push(source);
      });
    } else {
      console.log("removing all temp into source control");
      value = value.filter((source: any) => !this.tempSources.includes(source));
    }

    this.sourceControl.setValue(value);
    console.log("after changing all:", this.sourceControl.value);
  }

  submitUpdate(title: string, description: string) {
    if (!this.isRequiredError()) {
      console.log(this.sourceControl.value[0]);
      if (this.sourceControl.value.length === 1) {
        this.sourceService
          .HttpUpdateOneSource(
            this.sourceControl.value[0].source.uri,
            title,
            description
          )
          .subscribe((sources: UpdateResponse) => {
            console.log(sources);
            if (sources.ok === 0) {
              this.fireAlertService.fireDbError("update", "source");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess(
                "all sources updated successfully"
              );
              this.updateTitle(title, description);
              // alert('all sources updated successfully');
            }
          });
      } else {
        let lst: string[] = [];
        for (const source of this.sourceControl.value) {
          lst.push(source.source.uri);
        }

        this.sourceService
          .HttpUpdateManySources(lst, title, description)
          .subscribe((source: UpdateResponse) => {
            console.log(source);
            if (source.ok === 0) {
              this.fireAlertService.fireDbError("update", "sources");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess(
                "Wanted sources updated successfully"
              );
              this.updateTitle(title, description);
            }
          });
      }
    }
  }

  isRequiredError(): boolean {
    if (this.myForm.hasError("required", "title")) {
      return true;
    } else if (this.myForm.hasError("required", "description")) {
      return true;
    } else {
      return false;
    }
  }

  onDelete(source: Source): void {
    console.log("source is", source);
    const dialogRef = this.dialog.open(DeleteSourceDialogComponent, {
      width: "300px",
      height: "300px",
      data: {
        uri: source.uri,
        title: source.title,
        description: source.description,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log(`delete source:${source.title}!`);
        this.sourceService
          .HttpDeleteOneSource(source.uri)
          .subscribe((ok: number) => {
            if (ok === 0) {
              this.fireAlertService.fireDbError("delete", "source");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess("Source deleted successfully");
              // alert('sources deleted successfully');
              this.removeSourceFromList([source]);
              this.uncheckAll();
            }
          });
      } else {
        // this.fireAlertService.fireDeleteCanceled('Wanted source')
        console.log("user regret");
      }
    });
  }

  onDeleteAll() {
    const dialogRef = this.dialog.open(DeleteAllSourcesDialogComponent, {
      width: "325px",
      height: "250px",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log("delete all source:");
        console.log("source control:", this.sourceControl.value);
        let sources: string[] = [];
        for (const source of this.sourceControl.value) {
          sources.push(source.source.uri);
        }

        this.sourceService
          .HttpDeleteManySources(sources)
          .subscribe((ok: number) => {
            if (ok === 0) {
              this.fireAlertService.fireDbError("delete", "sourced");
              // alert('error occured');
            } else {
              this.fireAlertService.fireSuccess(
                "Wanted sources deleted successfully"
              );
              // alert('wanted sources deleted successfully');

              let sourcesToDel: Source[] = [];
              for (const sourceToDel of this.sourceControl.value) {
                sourcesToDel.push(sourceToDel.source);
              }
              console.log(sourcesToDel);
              this.removeSourceFromList(sourcesToDel);
              this.uncheckAll();
            }
          });
      } else {
        // this.fireAlertService.fireDeleteCanceled('Wanted sources')
        console.log("user regret");
      }
    });
  }

  onAddSource() {
    const dialogRef = this.dialog.open(AddSourceDialogComponent, {
      width: "300px",
      height: "450px",
    });

    dialogRef
      .afterClosed()
      .subscribe((result: { status: number; sourceAdded: Source }) => {
        if (result.status === 200) {
          console.log(result.sourceAdded);
          this.addSourceToList(result.sourceAdded);
        } else if (result.status === 201) {
          // this.fireAlertService.fireCreationCanceled('source')
          console.log("user regret");
        } else {
          this.fireAlertService.fireUnknownError();
          console.log("error occured");
        }
      });
  }

  addSourceToList(source: Source) {
    this.sources.push(new SourceSelect(source, false));

    this.sources.sort((a, b) => (a.source.title > b.source.title) ? 1 : -1);

    this.sourceService.AppendSource(source);
  }

  removeSourceFromList(sourcesToDelete: Source[]) {

    console.log('-----', sourcesToDelete, this.sources);
    for (const sourceDel of sourcesToDelete) {
      console.log("source", sourceDel);
      let i = this.findSource(sourceDel);
      console.log('found in index ', i)
      if (i != -1) {
        let sourceDiv: HTMLElement = document.getElementById("source-div-" + i);
        sourceDiv.classList.add("animation-delete");
        console.log(sourceDiv);
        setTimeout(() => {
          console.log("removed source:", this.sources.splice(i, 1));
        }, 300);
        // this.sources.splice(i, 1);
        this.sourceService.DeleteSource(sourceDel, i);
      }
    }

    console.log("source after slicing:\n", this.sources);
    console.log("source control is:", this.sourceControl.value);
    this.sourceControl.value.splice(0, this.sourceControl.value.length);
    console.log("after removing source control is:", this.sourceControl.value);
  }

  findSource(source: Source){
    for (let index = 0; index < this.sources.length; index++) {
      console.log(this.sources[index].source.title, source.title, this.sources[index].source.title === source.title)
      if (this.sources[index].source.title === source.title)
        return index;
    }

    return -1;
  }

  uncheckAll(){
    console.log("uncheck");
    this.tempSources.forEach((source: SourceSelect) => {
      let index: number = this.sources.findIndex(
        (x) => x.source.title === source.source.title
      );
      this.sources[index].selected = false;
      source.selected =  false;
      this.indeterminateSign = false;
      this.changeToSelectMode();
      console.log(source);
    });
  }

  updateTitle(title: string, description: string){
    console.log("update Title", this.sourceControl);
    
    this.sourceControl.value.forEach((source: SourceSelect) => {
      let index: number = this.sources.findIndex(
        (x) => x.source.title === source.source.title
      );

      this.sources[index].source.description = description;
      this.sources[index].source.title = title;
      source.source.title = title;
      source.source.description = description;

      console.log(source);
    });

    this.sortSources();
    this.sourceService.SortSources();
  }

  sortSources(){
    this.sources.sort((a, b) => (a.source.title > b.source.title) ? 1 : -1);
  }
}
