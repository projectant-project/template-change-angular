import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceManagementComponent } from './source-management.component';

describe('SourceManagementComponent', () => {
  let component: SourceManagementComponent;
  let fixture: ComponentFixture<SourceManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SourceManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
