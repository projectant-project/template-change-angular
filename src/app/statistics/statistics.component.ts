import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { Sensor } from '../interfaces/sensor.interface';
import { Source } from '../interfaces/source.interface';
import { Telemetry } from '../interfaces/telemetry.interface';
import { FireAlertService } from '../services/fire-alert.service';
import { TelemetryService } from '../services/telemetry.service';
import { Range } from '../interfaces/range.interface';
import { SourceService } from '../services/source.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  public lineBigDashboardChartType;
  public gradientStroke;
  public chartColor;
  public canvas : any;
  public ctx;
  public gradientFill;
  public lineBigDashboardChartData:Array<any>;
  public lineBigDashboardChartOptions:any;
  public lineBigDashboardChartLabels:Array<any>;
  public lineBigDashboardChartColors:Array<any>;
  private lineBigSensor: string;
  private lineBigSource: string;
  private chart: Chart;
  
  public gradientChartOptionsConfiguration: any;
  public gradientChartOptionsConfigurationWithNumbersAndGrid: any;
  public gradientBarChartOptionsConfigurationWithNumbersAndGrid: any;
  
  public lineChartType;
  public lineChartData:Array<any>;
  public lineChartOptions:any;
  public lineChartLabels:Array<any>;
  public lineChartColors:Array<any>;
  private lineChartSensor: string;
  private lineChartSource: string;
  
  public lineChartWithNumbersAndGridType;
  public lineChartWithNumbersAndGridData:Array<any>;
  public lineChartWithNumbersAndGridOptions:any;
  public lineChartWithNumbersAndGridLabels:Array<any>;
  public lineChartWithNumbersAndGridColors:Array<any>;
  private lineChartWithNumbersAndGridSensor: string;
  private lineChartWithNumbersAndGridSource: string;
  
  public lineChartGradientsNumbersType;
  public lineChartGradientsNumbersData:Array<any>;
  public lineChartGradientsNumbersOptions:any;
  public lineChartGradientsNumbersLabels:Array<any>;
  public lineChartGradientsNumbersColors:Array<any>;
  private lineChartGradientsNumbersSensor: string;
  private lineChartGradientsNumbersSource: string;
  
  // big chart filters
  device: string = "";
  key: string = "";
  name: string = "";
  time: Range = {time: '', full: ''};
  stop: string = "now";
  startRange: Range[] = [];
  sources: Source[] = [];
  sensors: Sensor[] = [];
  sourcesLst: Source[];
  comps: string[] = ["Lat.", "Long.", "Alt.", "2D speed"]

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
  public hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
      g = parseInt(hex.slice(3, 5), 16),
      b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
      return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
      return "rgb(" + r + ", " + g + ", " + b + ")";
    }
  }
  constructor(private telemetryService: TelemetryService, private sourceService: SourceService, private fireAlertService: FireAlertService) { }

  ngOnInit() {

    // set preferences
    this.startRange = this.telemetryService.GetStartRanges();
    this.sensors = this.telemetryService.GetDefaultSensors();
    this.sources = this.sourceService.getSources();
    this.time = this.startRange[this.startRange.length - 1];
    this.getHighestPrioritySensor();
    this.getHighestPrioritySource();

    // graphs
    this.lineBigSource = 'hero8';
    this.lineBigSensor = 'Accelerometer';
    this.initBigGraph();

    this.generateOptions();
    
    this.lineChartSource = 'hero8';
    this.lineChartSensor = 'Accelerometer';
    this.initLineChart();

    this.lineChartWithNumbersAndGridSource = 'hero7';
    this.lineChartWithNumbersAndGridSensor = 'Latitue';
    this.initLineChartWithNumbers();

    this.lineChartGradientsNumbersSource = 'hero6';
    this.lineChartGradientsNumbersSensor = 'Altitue';
    this.initBarChart();

  }

  generateOptions(){

    this.gradientChartOptionsConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        bodySpacing: 4,
        mode: "nearest",
        intersect: 0,
        position: "nearest",
        xPadding: 10,
        yPadding: 10,
        caretPadding: 10
      },
      responsive: true,
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'm/s²'
          },
          gridLines: {
            zeroLineColor: "transparent",
            drawBorder: false
          },
          ticks: {
              stepSize: 500
          }
        }],
        xAxes: [{
          display: 0,
          ticks: {
            display: false
          },
          gridLines: {
            zeroLineColor: "transparent",
            drawTicks: false,
            display: false,
            drawBorder: false
          }
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 15,
          bottom: 15
        }
      }
    };

    this.gradientChartOptionsConfigurationWithNumbersAndGrid = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        bodySpacing: 4,
        mode: "nearest",
        intersect: 0,
        position: "nearest",
        xPadding: 10,
        yPadding: 10,
        caretPadding: 10
      },
      responsive: true,
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'deg'
          },
          gridLines: {
            zeroLineColor: "transparent",
            drawBorder: false
          },
          ticks: {
              stepSize: 500
          }
        }],
        xAxes: [{
          display: 0,
          ticks: {
            display: false
          },
          gridLines: {
            zeroLineColor: "transparent",
            drawTicks: false,
            display: false,
            drawBorder: false
          }
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 15,
          bottom: 15
        }
      }
    };

    // this.gradientBarChartOptionsConfigurationWithNumbersAndGrid = {
    //   maintainAspectRatio: false,
    //   legend: {
    //     display: false
    //   },
    //   tooltips: {
    //     bodySpacing: 4,
    //     mode: "nearest",
    //     intersect: 0,
    //     position: "nearest",
    //     xPadding: 10,
    //     yPadding: 10,
    //     caretPadding: 10
    //   },
    //   responsive: true,
    //   scales: {
    //     yAxes: [{
    //       scaleLabel: {
    //         display: true,
    //         labelString: ''
    //       },
    //       gridLines: {
    //         zeroLineColor: "transparent",
    //         drawBorder: false
    //       },
    //       ticks: {
    //           stepSize: 500
    //       }
    //     }],
    //     xAxes: [{
    //       display: 0,
    //       ticks: {
    //         display: false
    //       },
    //       gridLines: {
    //         zeroLineColor: "transparent",
    //         drawTicks: false,
    //         display: false,
    //         drawBorder: false
    //       }
    //     }]
    //   },
    //   layout: {
    //     padding: {
    //       left: 0,
    //       right: 0,
    //       top: 15,
    //       bottom: 15
    //     }
    //   }
    // };

    this.gradientBarChartOptionsConfigurationWithNumbersAndGrid = {
      maintainAspectRatio: false,
          legend: {
            display: false
          },
          tooltips: {
            bodySpacing: 4,
            mode: "nearest",
            intersect: 0,
            position: "nearest",
            xPadding: 10,
            yPadding: 10,
            caretPadding: 10
          },
          responsive: 1,
          scales: {
            yAxes: [{
              gridLines: {
                zeroLineColor: "transparent",
                drawBorder: false
              },
              ticks: {
                  stepSize: 20
              },
              scaleLabel: {
                display: true,
                labelString: 'm'
              },
            }],
            xAxes: [{
              display: 0,
              ticks: {
                display: false
              },
              gridLines: {
                zeroLineColor: "transparent",
                drawTicks: false,
                display: false,
                drawBorder: false
              }
            }]
          },
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 15,
              bottom: 15
            }
          }
    }
  }

  initLineChart(){
    this.canvas = document.getElementById("lineChartExample");
    this.ctx = this.canvas.getContext("2d");

    this.gradientStroke = this.ctx.createLinearGradient(500, 0, 100, 0);
    this.gradientStroke.addColorStop(0, '#80b6f4');
    this.gradientStroke.addColorStop(1, this.chartColor);

    this.gradientFill = this.ctx.createLinearGradient(0, 170, 0, 50);
    this.gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    this.gradientFill.addColorStop(1, "rgba(249, 99, 59, 0.40)");

    this.lineChartData = [
        {
          label: "Accelerometer (AVG)",
          pointBorderWidth: 2,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 1,
          pointRadius: 4,
          fill: true,
          borderWidth: 2,
          data: []
          // data: [542, 480, 430, 550, 530, 453, 380, 434, 568, 610, 700, 630]
        }
    ];

    this.lineChartColors = [
       {
         borderColor: "#f96332",
         pointBorderColor: "#FFF",
         pointBackgroundColor: "#f96332",
         backgroundColor: this.gradientFill
       }
    ];
    
    this.lineChartOptions = this.gradientChartOptionsConfiguration;

    // this.lineChartLabels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    // console.log(this.lineChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString)
    this.lineChartLabels = [];

    this.changeLine();

    this.lineChartType = 'line';
  }

  initLineChartWithNumbers(){
    this.canvas = document.getElementById("lineChartExampleWithNumbersAndGrid");
    this.ctx = this.canvas.getContext("2d");

    this.gradientStroke = this.ctx.createLinearGradient(500, 0, 100, 0);
    this.gradientStroke.addColorStop(0, '#18ce0f');
    this.gradientStroke.addColorStop(1, this.chartColor);

    this.gradientFill = this.ctx.createLinearGradient(0, 170, 0, 50);
    this.gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    this.gradientFill.addColorStop(1, this.hexToRGB('#18ce0f', 0.4));

    this.lineChartWithNumbersAndGridData = [
        {
          label: "Latitue",
           pointBorderWidth: 2,
           pointHoverRadius: 4,
           pointHoverBorderWidth: 1,
           pointRadius: 4,
           fill: true,
           borderWidth: 2,
           data: []
          // data: [40, 500, 650, 700, 1200, 1250, 1300, 1900]
        }
      ];
      this.lineChartWithNumbersAndGridColors = [
       {
         borderColor: "#18ce0f",
         pointBorderColor: "#FFF",
         pointBackgroundColor: "#18ce0f",
         backgroundColor: this.gradientFill
       }
     ];
     this.lineChartWithNumbersAndGridOptions = this.gradientChartOptionsConfigurationWithNumbersAndGrid;
     this.lineChartWithNumbersAndGridLabels = [];
    // this.lineChartWithNumbersAndGridLabels = ["12pm,", "3pm", "6pm", "9pm", "12am", "3am", "6am", "9am"];

    this.changeLineWithNumbers();

    this.lineChartWithNumbersAndGridType = 'line';
  }

  initBarChart(){
    this.canvas = document.getElementById("barChartSimpleGradientsNumbers");
    this.ctx = this.canvas.getContext("2d");

    this.gradientFill = this.ctx.createLinearGradient(0, 170, 0, 50);
    this.gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    this.gradientFill.addColorStop(1, this.hexToRGB('#2CA8FF', 0.6));


    this.lineChartGradientsNumbersData = [
        {
          label: "Altitue",
          pointBorderWidth: 2,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 1,
          pointRadius: 4,
          fill: true,
          borderWidth: 1,
          data: []
          // data: [80, 99, 86, 96, 123, 85, 100, 75, 88, 90, 123, 155]
        }
      ];
    this.lineChartGradientsNumbersColors = [
     {
       backgroundColor: this.gradientFill,
       borderColor: "#2CA8FF",
       pointBorderColor: "#FFF",
       pointBackgroundColor: "#2CA8FF",
     }
   ];
   this.lineChartGradientsNumbersOptions = this.gradientBarChartOptionsConfigurationWithNumbersAndGrid;
   this.lineChartGradientsNumbersLabels = [];

    // this.lineChartGradientsNumbersLabels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    // this.lineChartGradientsNumbersOptions = {
    //     maintainAspectRatio: false,
    //     legend: {
    //       display: false
    //     },
    //     tooltips: {
    //       bodySpacing: 4,
    //       mode: "nearest",
    //       intersect: 0,
    //       position: "nearest",
    //       xPadding: 10,
    //       yPadding: 10,
    //       caretPadding: 10
    //     },
    //     responsive: 1,
    //     scales: {
    //       yAxes: [{
    //         gridLines: {
    //           zeroLineColor: "transparent",
    //           drawBorder: false
    //         },
    //         ticks: {
    //             stepSize: 20
    //         },
    //         scaleLabel: {
    //           display: true,
    //           labelString: 'm'
    //         },
    //       }],
    //       xAxes: [{
    //         display: 0,
    //         ticks: {
    //           display: false
    //         },
    //         gridLines: {
    //           zeroLineColor: "transparent",
    //           drawTicks: false,
    //           display: false,
    //           drawBorder: false
    //         }
    //       }]
    //     },
    //     layout: {
    //       padding: {
    //         left: 0,
    //         right: 0,
    //         top: 15,
    //         bottom: 15
    //       }
    //     }
    //   }

    this.changeBarWithNumbers();
    
    this.lineChartGradientsNumbersType = 'line';
  }

  initBigGraph(){
    this.chartColor = "#FFFFFF";
    this.canvas = document.getElementById("bigDashboardChart");
    this.ctx = this.canvas.getContext("2d");

    this.gradientStroke = this.ctx.createLinearGradient(500, 0, 100, 0);
    this.gradientStroke.addColorStop(0, '#80b6f4');
    this.gradientStroke.addColorStop(1, this.chartColor);

    this.gradientFill = this.ctx.createLinearGradient(0, 200, 0, 50);
    this.gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    this.gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    this.lineBigDashboardChartData = [
        {
          label: "data",

          pointBorderWidth: 1,
          pointHoverRadius: 7,
          pointHoverBorderWidth: 2,
          pointRadius: 5,
          fill: true,

          borderWidth: 2,
          data: [50, 150, 100, 190, 130, 90, 150, 160, 120, 140, 190, 95]
        }
    ];

    this.lineBigDashboardChartColors = [
       {
         backgroundColor: this.gradientFill,
         borderColor: this.chartColor,
         pointBorderColor: this.chartColor,
         pointBackgroundColor: "#2c2c2c",
         pointHoverBackgroundColor: "#2c2c2c",
         pointHoverBorderColor: this.chartColor,
       }
    ];

    this.lineBigDashboardChartLabels = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    
    this.lineBigDashboardChartOptions = {

          layout: {
              padding: {
                  left: 20,
                  right: 20,
                  top: 0,
                  bottom: 0
              }
          },
          maintainAspectRatio: false,
          tooltips: {
            backgroundColor: '#fff',
            titleFontColor: '#333',
            bodyFontColor: '#666',
            bodySpacing: 4,
            xPadding: 12,
            mode: "nearest",
            intersect: 0,
            position: "nearest"
          },
          legend: {
              position: "bottom",
              fillStyle: "#FFF",
              display: false,
          },
          scales: {
              yAxes: [{
                  ticks: {
                      fontColor: "rgba(255,255,255,0.4)",
                      fontStyle: "bold",
                      beginAtZero: true,
                      maxTicksLimit: 5,
                      padding: 10
                  },
                  gridLines: {
                      drawTicks: true,
                      drawBorder: false,
                      display: true,
                      color: "rgba(255,255,255,0.1)",
                      zeroLineColor: "transparent"
                  },
                  scaleLabel: {
                    display: true,
                  }

              }],
              xAxes: [{
                  gridLines: {
                      zeroLineColor: "transparent",
                      display: false,

                  },
                  ticks: {
                      padding: 10,
                      fontColor: "rgba(255,255,255,0.4)",
                      fontStyle: "bold"
                  }
              }]
          }
    };

    this.lineBigDashboardChartType = 'line';
    
    this.getTelemetryByFlters();
  }


  getHighestPrioritySource(){

    const priority = this.sources.filter((source) => source.title.startsWith("hero"));
    console.log(priority);

    if (priority.length === 0){
      console.log('NOT FOUND !!')
      this.device = this.sources[0].title;
    }
    else{
      console.log('PRIORITY FOUND !!')
      this.device = priority[priority.length - 1].title;
    }
  }

  getHighestPrioritySensor(){
    // const priority = this.sources.filter((source) => source.title.startsWith("hero"));
    const highestPrioritySensors = ['Lat.', 'Alt.', 'Shutter speed'] 
    const priority = this.sensors.filter((sensor) => highestPrioritySensors.includes(sensor.name));
    console.log(priority);

    if (priority.length === 0){
      console.log('NOT FOUND !!')
      this.key = this.sensors[0].key;
      this.name = this.sensors[0].name;
    }
    else{
      console.log('PRIORITY FOUND !!')
      this.key =  priority[0].key;
      this.name =  priority[0].name;
    }
  }

  getKeyByName(){
    for (let index = 0; index < this.sensors.length; index++) {
      const element = this.sensors[index];
      if (element.name === this.name)
        this.key = element.key      
    }
  }

  telemetryToDatas(telemetryItems: Telemetry[], key: string){
    console.log(telemetryItems);
    let data = telemetryItems[0].value.split(', ')
    let value = data[0].replace(/\D+$/g, "")
    let units = data[0].substring(value.length);
    console.log(data, data[0], value, units)
    
    // generate axis
    let axisName: string;
    if (key === 'GPS5' || key === 'SHUT')
      axisName = telemetryItems[0].name;
    else
      axisName = `${telemetryItems[0].name}-AVG`;

    
    let series = []
    
    // init datas
    telemetryItems.map((item: Telemetry) => {
      let data = item.value.split(', ');
      let value;
      let avg = 0;

      // console.log(data);

      if (data.length === 1)
        value = item.value.replace(/\D+$/g, "");
      else{
        data.forEach(element => {
          element = element.replace(/\D+$/g, "")
          avg += Number(element);
        });

        avg = avg / data.length;
        value = avg;
      }

      // console.log(avg);
      
      series.push({
        "name": item.time,
        "value": value
      })  
    })

    console.log(series);
    return {sensorName: axisName, unit: units, data: series};
  }

  onChangeSource(event: Event){
    console.log(event);
    // this.getSensorsByDevice(this.device);
    console.log(this.time);

    if (this.device != "" && this.key != "")
    {
      console.log(`device=${this.device}, key=${this.key}, name=${this.name}, time=${this.time.time}`)
      this.getTelemetryByFlters();
    }
  }

  onChangeSensor(event: Event){
    console.log(event);

    console.log(this.time, this.name)

    this.getKeyByName();

    if (this.time.time != '')
    {
      console.log(`device=${this.device}, key=${this.key}, name=${this.name}, time=${this.time.time}`)
      this.getTelemetryByFlters();
    }
  }

  onChangeRange(event: Event){
    console.log(event);
    console.log(this.time);

    if (this.device != "" && this.key != "")
    {
      console.log(`device=${this.device}, key=${this.key}, name=${this.name}, time=${this.time.time}`)
      this.getTelemetryByFlters();
    }
  }

  // onChange(){
  //   // this.lineChartData[0].data = [600, 600, 600, 600, 530, 453, 380, 434, 568, 610, 700, 630];

  //   let filters = {name: 'Accelerometer', time: '1m', stop: 'now'};
  //   this.telemetryService.HttpTelemetryGet('hero8', 'ACCL', filters).subscribe((telemetryItems: Telemetry[]) => {
  //     console.log('-- SUCCESSFUL GET REQUEST !! --')
  //     console.log(telemetryItems, telemetryItems === []);
  //     if (telemetryItems.length > 0){
  //       alert('work');
  //       let data: {sensorName: string, unit: string, data: any[]};
  //       data = this.telemetryToDatas(telemetryItems, 'ACCL');
  //       this.updateGraph(data.data, data.sensorName, data.unit);
  //     }
  //     else
  //       this.fireAlertService.fireNoData();
  //   }, (error: Error) => {
  //     console.log('Some error occured', error)
  //   });   
  // }

  getTelemetryByFlters()
  {
    let filters = {name: this.name, time: this.time.time, stop: this.stop}
    this.telemetryService.HttpTelemetryGet(this.device, this.key, filters).subscribe((telemetryItems: Telemetry[]) => {
      console.log('-- SUCCESSFUL GET REQUEST !! --')
      console.log(telemetryItems, telemetryItems === []);
      if (telemetryItems.length > 0)
      {
        let data: {sensorName: string, unit: string, data: any[]};
        data = this.telemetryToDatas(telemetryItems, this.key);
        this.updateGraph(data.data, data.sensorName, data.unit);
      }
      else
        this.fireAlertService.fireNoData();
    }, (error: Error) => {
      console.log('Some error occured', error)
    });
  }

  // bigGraphDashboardData(){
    
  //   this.lineBigDashboardChartData[0].data = [150, 150, 150, 190, 130, 90, 150, 160, 120, 140, 190, 95];   
  //   this.lineBigDashboardChartData[0].label = this.lineBigSensor;

  //   this.lineBigDashboardChartLabels = ["JAN", "JAN", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    
  //   console.log(this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'])
  //   console.log(this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString)

  //   // this.lineBigDashboardChartOptions['legend'].display = true;
  //   this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString = "m/s"
  // }

  updateGraph(items: any[], sensorName: string, unit: string){

    console.log('GRAPHS DETAILS --', items, sensorName, unit);
    let yAxes = [];
    let xAxes = [];

    items.forEach(element => {
      yAxes.push(element.value);
      xAxes.push(element.name);
    });

    this.lineBigDashboardChartData[0].data = yAxes; 
    this.lineBigDashboardChartLabels = xAxes;
    this.lineBigDashboardChartData[0].label = sensorName;
    // this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'].display = true;
    // this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString = unit;
    this.lineBigDashboardChartOptions.scales.yAxes[0].scaleLabel.display = true;
    this.lineBigDashboardChartOptions.scales.yAxes[0].scaleLabel.labelString = unit;

    console.log(this.lineBigDashboardChartOptions.scales.yAxes[0].scaleLabel)
    console.log(this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'])
    console.log(this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString)

  }

  changeLine(){
    let source = 'hero8';
    let key = 'ACCL';
    let filters = {name: 'Accelerometer', time: '1m', stop: 'now'};
    this.telemetryService.HttpTelemetryGet(source, key, filters).subscribe((telemetryItems: Telemetry[]) => {
      console.log('-- SUCCESSFUL GET REQUEST !! --')
      console.log(telemetryItems, telemetryItems === []);
      if (telemetryItems.length > 0){
        let data: {sensorName: string, unit: string, data: any[]};
        data = this.telemetryToDatas(telemetryItems, key);
        this.updateLine(data.data, data.sensorName, data.unit);
      }
      else
        this.fireAlertService.fireNoData();
    }, (error: Error) => {
      console.log('Some error occured', error)
    }); 
  }

  updateLine(items: any[], sensorName: string, unit: string){

    let yAxes = [];
    let xAxes = [];

    items.forEach(element => {
      yAxes.push(element.value);
      xAxes.push(element.name);
    });

    this.lineChartData[0].data = yAxes; 
    this.lineChartLabels = xAxes;
    // this.lineChartData[0].label = sensorName;

    // console.log(this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'])
    // console.log(this.lineBigDashboardChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString)

    // this.lineBigDashboardChartOptions['legend'].display = true;
    // this.lineChartOptions['scales']['yAxes'][0]['scaleLabel'].labelString = unit;
  }



  changeLineWithNumbers(){
    let source = 'hero8';
    let key = 'GPS5';
    let filters = {name: 'Lat.', time: '1m', stop: 'now'};
    this.telemetryService.HttpTelemetryGet(source, key, filters).subscribe((telemetryItems: Telemetry[]) => {
      console.log('-- SUCCESSFUL GET REQUEST !! --')
      console.log(telemetryItems, telemetryItems === []);
      if (telemetryItems.length > 0){
        let data: {sensorName: string, unit: string, data: any[]};
        data = this.telemetryToDatas(telemetryItems, key);
        this.updateLineWithNumbers(data.data, data.sensorName, data.unit);
      }
      else
        this.fireAlertService.fireNoData();
    }, (error: Error) => {
      console.log('Some error occured', error)
    }); 
  }

  updateLineWithNumbers(items: any[], sensorName: string, unit: string){

    console.log(sensorName, unit);

    let yAxes = [];
    let xAxes = [];

    items.forEach(element => {
      yAxes.push(element.value);
      xAxes.push(element.name);
    });

    this.lineChartWithNumbersAndGridData[0].data = yAxes; 
    this.lineChartWithNumbersAndGridLabels = xAxes;
    // this.lineChartWithNumbersAndGridData[0].label = sensorName;

    // console.log(this.lineChartWithNumbersAndGridOptions['scales']['yAxes'][0]['scaleLabel'])
    // console.log(this.lineChartWithNumbersAndGridOptions['scales']['yAxes'][0]['scaleLabel'].labelString)

    // // this.lineBigDashboardChartOptions['legend'].display = true;
    // this.lineChartWithNumbersAndGridOptions['scales']['yAxes'][0]['scaleLabel'].labelString = unit;
    // console.log(this.lineChartWithNumbersAndGridOptions['scales']['yAxes'][0]['scaleLabel'].labelString)
  }
 


  changeBarWithNumbers(){
    let source = 'hero8';
    let key = 'GPS5';
    let filters = {name: 'Alt.', time: '1m', stop: 'now'};
    this.telemetryService.HttpTelemetryGet(source, key, filters).subscribe((telemetryItems: Telemetry[]) => {
      console.log('-- SUCCESSFUL GET REQUEST !! --')
      console.log(telemetryItems, telemetryItems === []);
      if (telemetryItems.length > 0){
        let data: {sensorName: string, unit: string, data: any[]};
        data = this.telemetryToDatas(telemetryItems, key);
        this.updateBarWithNumbers(data.data, data.sensorName, data.unit);
      }
      else
        this.fireAlertService.fireNoData();
    }, (error: Error) => {
      console.log('Some error occured', error)
    }); 
  }

  updateBarWithNumbers(items: any[], sensorName: string, unit: string){

    console.log(sensorName, unit);

    let yAxes = [];
    let xAxes = [];

    items.forEach(element => {
      yAxes.push(element.value);
      xAxes.push(element.name);
    });

    this.lineChartGradientsNumbersData[0].data = yAxes; 
    this.lineChartGradientsNumbersLabels = xAxes;
    // this.lineChartGradientsNumbersData[0].label = sensorName;

    // console.log(this.lineChartGradientsNumbersOptions['scales']['yAxes'][0]['scaleLabel'])
    // console.log(this.lineChartGradientsNumbersOptions['scales']['yAxes'][0]['scaleLabel'].labelString)

    // // this.lineBigDashboardChartOptions['legend'].display = true;
    // this.lineChartGradientsNumbersOptions['scales']['yAxes'][0]['scaleLabel'].labelString = unit;
    // console.log(this.lineChartGradientsNumbersOptions['scales']['yAxes'][0]['scaleLabel'].labelString)
  }


}

